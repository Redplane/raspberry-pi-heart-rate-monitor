using System;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using ElectronNET.API;
using ElectronNET.API.Entities;
using HeartRateMonitor.Extensions;
using HeartRateMonitor.HostedServices;
using HeartRateMonitor.Hubs;
using HeartRateMonitor.Models;
using HeartRateMonitor.Services;
using HeartRateMonitor.Services.Interfaces;
using LiteMessageBus.Services.Implementations;
using LiteMessageBus.Services.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using uPLibrary.Networking.M2Mqtt;

namespace HeartRateMonitor
{
	public class Startup
	{
		#region Constructor

		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		#endregion

		#region Properties

		public IConfiguration Configuration { get; }

		#endregion

		#region Methods

		// This method gets called by the runtime. Use this method to add services to the container.
		// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
		public void ConfigureServices(IServiceCollection services)
		{
			// Request validation.
			services.AddScoped(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehavior<,>));

			var iotHubSettings = new IotHubSettings();
			Configuration.GetSection(nameof(IotHubSettings)).Bind(iotHubSettings);
			services.AddSingleton(iotHubSettings);

			var caCert =
				X509Certificate.CreateFromCertFile(Path.Join(AppContext.BaseDirectory, "certificates",
					"AmazonRootCA1.crt"));
			var clientCert =
				new X509Certificate2(Path.Join(AppContext.BaseDirectory, "certificates", "certificate.cert.pfx"),
					"123456");

			var client = new MqttClient(iotHubSettings.Endpoint, iotHubSettings.BrokerPort, true, caCert, clientCert,
				MqttSslProtocols.TLSv1_2);
			client.Connect(Guid.NewGuid().ToString());
			services.AddSingleton(client);

			Console.WriteLine("IotHub has been connected with settings: ");
			Console.WriteLine(JsonConvert.SerializeObject(iotHubSettings));

			services.AddSingleton<ISerialPortService, SerialPortService>();
			services.AddSingleton<IEcgService, EcgService>();
			services.AddSingleton<IMeasurementProcessService, MeasurementProcessService>();
			services.AddSingleton<ILiteMessageBusService, InMemoryLiteMessageBusService>();
			services.AddSingleton<IMeasurementService, MeasurementService>();

			services.AddHostedService<MeasurementHostedService>();

			services.AddMediatR(typeof(Startup).Assembly);

			// Add CORS.
			services.AddCors();

			// Add signalr support.
			services.AddSignalR();

			services.AddControllersWithViews()
				.AddNewtonsoftJson(options =>
				{
					var camelCasePropertyNamesContractResolver = new CamelCasePropertyNamesContractResolver();
					options.SerializerSettings.ContractResolver = camelCasePropertyNamesContractResolver;
				});

			// In production, the Angular files will be served from this directory
			services.AddSpaStaticFiles(configuration => { configuration.RootPath = "ClientApp/dist"; });
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
				app.UseDeveloperExceptionPage();

			app.UseCustomExceptionHandler(env);

			// Open the Electron-Window here
			var browserWindowOptions = new BrowserWindowOptions();
			browserWindowOptions.TitleBarStyle = TitleBarStyle.hidden;
			browserWindowOptions.AutoHideMenuBar = true;
			browserWindowOptions.WebPreferences = new WebPreferences();
			browserWindowOptions.WebPreferences.DevTools = true;

			app.UseCors(options => options
				.AllowAnyHeader()
				.WithMethods("GET", "POST", "PUT", "DELETE")
				.WithOrigins("http://localhost:4200", "http://45.119.213.119:22021", "http://45.119.213.119:22020",
					"http://localhost:22021", "http://localhost:55308")
				.AllowCredentials()
			);

			// Use routing.
			app.UseRouting();

			//var currentPath = Directory.GetCurrentDirectory();
			//Console.WriteLine(currentPath);

			// Use static file.
			app.UseStaticFiles();
			if (!env.IsDevelopment())
				app.UseSpaStaticFiles();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllerRoute(
					"default",
					"{controller=Home}/{action=Index}/{id?}");
				
				endpoints.MapHub<MeasurementHub>("/signalr-hub/measurement");
			});

			var iotHubSettings = app.ApplicationServices.GetService<IotHubSettings>();

			app.UseSpa(spa =>
			{
				// To learn more about options for serving an Angular SPA from ASP.NET Core,
				// see https://go.microsoft.com/fwlink/?linkid=864501

				spa.Options.SourcePath = "ClientApp";

				if (env.IsDevelopment())
					spa.UseAngularCliServer("start");
			});

			if (HybridSupport.IsElectronActive)
			{
				Task.Run(async () =>
				{
					var browserWindow = await Electron.WindowManager.CreateWindowAsync(browserWindowOptions);
					Console.WriteLine($"Start url at: {iotHubSettings.StartupUrl}");
					//browserWindow.LoadURL($"{serverAddress}/index.html");
					browserWindow.LoadURL(iotHubSettings.StartupUrl);
				
					browserWindow.Show();
				});
			}

			
		}

		#endregion
	}
}