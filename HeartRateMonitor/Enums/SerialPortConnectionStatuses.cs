﻿namespace HeartRateMonitor.Enums
{
	public enum SerialPortConnectionStatuses
	{
		Unknown,
		Connected,
		Disconnected
	}
}