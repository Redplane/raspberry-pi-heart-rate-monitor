﻿namespace HeartRateMonitor.Enums
{
    public enum MeasurementProcessStatuses
    {
        Unavailable,
        Running,
        Stopped
    }
}