import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Inject, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {finalize, tap} from 'rxjs/operators';
import {INgRxMessageBusService, MESSAGE_BUS_SERVICE_PROVIDER} from 'ngrx-message-bus';

// Toggle application loader when application makes | cancels http requests.
@Injectable()
export class HttpRequestLoaderUiInterceptor implements HttpInterceptor {

  //#region Constructor

  public constructor(protected router: Router,
                     @Inject(MESSAGE_BUS_SERVICE_PROVIDER) protected messageBusService: INgRxMessageBusService) {
  }

  //#endregion


  /*
  * Intercept the out going request to add authorize header which retrieved from storage.
  * */
  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const clonedRequest = request.clone();

    // this.messageBusService.addMessage<boolean>(WebMessageBusChannelNameConstant.ui,
    //   WebMessageBusEventNameConstant.toggleAppLoader, true);

    return next.handle(clonedRequest);
      // .pipe(
      //   finalize(() => {
      //     this.messageBusService.addMessage<boolean>(WebMessageBusChannelNameConstant.ui,
      //       WebMessageBusEventNameConstant.toggleAppLoader, false);
      //   })
      // );
  }

}
