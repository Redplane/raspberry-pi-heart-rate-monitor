import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Inject, Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {catchError, flatMap} from 'rxjs/operators';
import {ScreenCodeConstant} from '../constants/screen-code.constant';
import {AUTHENTICATION_SERVICE_INJECTOR, NAVIGATION_SERVICE_PROVIDER} from '../constants/injection-token.constant';
import {INavigationService} from '../services/interfaces/navigation-service.interface';
import {IAuthenticationService} from '../services/interfaces/authentication-service.interface';

@Injectable()
export class UnauthorizedResponseInterceptor implements HttpInterceptor {

    //#region Constructor

    public constructor(@Inject(NAVIGATION_SERVICE_PROVIDER) protected navigationService: INavigationService,
                       @Inject(AUTHENTICATION_SERVICE_INJECTOR) protected authenticationService: IAuthenticationService) {
    }

    //#endregion

    //#region Methods

    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const clonedRequest = req.clone();
        return next.handle(clonedRequest)
            .pipe(
                catchError((error, caught) => {

                    if (error.status !== 401) {
                        return throwError(error);
                    }

                    // Get current screen code.
                    const screenCode = this.navigationService
                        .getCurrentScreenCode();

                    if (screenCode === ScreenCodeConstant.login) {
                        return throwError(error);
                    }

                    return this.authenticationService
                        .isLogoutRequestAsync(req)
                        .pipe(
                            flatMap((isLogoutRequest: boolean) => {
                                if (isLogoutRequest) {
                                    return throwError(error);
                                }

                                return this.authenticationService
                                    .logOutAsync()
                                    .pipe(
                                        flatMap(_ => this.navigationService
                                            .navigateToScreenAsync(ScreenCodeConstant.login)),
                                        flatMap(_ => throwError(error))
                                    );
                            })
                        );
                })
            );
    }

    //#endregion
}
