import {NgModule} from '@angular/core';
import {AuthorizeHeaderInterceptor} from './authorize-header.interceptor';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {ServicesModule} from '../services/services.module';
import {RouterModule} from '@angular/router';
import {HttpRequestLoaderUiInterceptor} from './http-request-loader-ui.interceptor';
import {UnauthorizedResponseInterceptor} from './unauthorized-response.interceptor';
import {HTTP_EXCEPTION_MESSAGE_HANDLER} from '../constants/injection-token.constant';
import {TranslateModule} from '@ngx-translate/core';
import {HttpExceptionToastHandlerInterceptor} from './http-exception-toast-handler.interceptor';

@NgModule({
    imports: [
        ServicesModule,
        RouterModule,
        TranslateModule.forChild()
    ],
    declarations: [],
    providers: [
        {provide: HTTP_INTERCEPTORS, useClass: HttpExceptionToastHandlerInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: AuthorizeHeaderInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: HttpRequestLoaderUiInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: UnauthorizedResponseInterceptor, multi: true},
    ]
})
export class InterceptorsModule {
}
