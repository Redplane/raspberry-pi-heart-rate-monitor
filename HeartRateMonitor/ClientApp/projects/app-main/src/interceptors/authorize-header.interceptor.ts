import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {forkJoin, from, Observable, of, throwError} from 'rxjs';
import {StorageMap} from '@ngx-pwa/local-storage';
import {LoginResultViewModel} from '../view-models/login-result.view-model';
import {LocalStorageKeyConstant} from '../constants/local-storage-key.constant';
import {catchError, flatMap} from 'rxjs/operators';
import {Inject, Injectable, Injector} from '@angular/core';
import {Router} from '@angular/router';
import {IAuthenticationService} from '../services/interfaces/authentication-service.interface';
import {AUTHENTICATION_SERVICE_INJECTOR, NAVIGATION_SERVICE_PROVIDER} from '../constants/injection-token.constant';
import {INavigationService} from '../services/interfaces/navigation-service.interface';
import {ScreenCodeConstant} from '../constants/screen-code.constant';

@Injectable()
export class AuthorizeHeaderInterceptor implements HttpInterceptor {

    //#region Constructor

    public constructor(protected storageMap: StorageMap,
                       @Inject(AUTHENTICATION_SERVICE_INJECTOR) protected authenticationService: IAuthenticationService,
                       @Inject(NAVIGATION_SERVICE_PROVIDER) protected navigationService: INavigationService,
                       protected router: Router) {
    }

    //#endregion


    /*
    * Intercept the out going request to add authorize header which retrieved from storage.
    * */
    public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        return this.storageMap
            .get<LoginResultViewModel>(LocalStorageKeyConstant.loginResult)
            .pipe(
                flatMap((loginResult: LoginResultViewModel) => {

                    if (!loginResult || !loginResult.accessToken) {
                        return next
                            .handle(request);
                    }

                    const headers = request.headers.set('Authorization', `${loginResult.accessToken}`)
                        .set('Cache-Control', 'no-cache')
                        .set('Cache-Control', 'no-store')
                        .set('Expires', '0')
                        .set('Pragma', 'no-cache');


                    const clonedRequest = request.clone({
                        headers
                    });

                    return next
                        .handle(clonedRequest);
                })
            );
    }

}
