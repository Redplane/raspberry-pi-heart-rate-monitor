import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {Injectable, Injector} from '@angular/core';
import {catchError} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {InvalidBusinessResponseViewModel} from '../view-models/invalid-business-response.view-model';

@Injectable()
export class HttpExceptionToastHandlerInterceptor implements HttpInterceptor {

    //#region Constructor

    public constructor(protected injector: Injector) {
    }

    //#endregion

    //#region Methods

    public intercept(httpRequest: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const clonedRequest = httpRequest.clone();

        if (clonedRequest.url.indexOf('/api/') < 0) {
            return next.handle(clonedRequest);
        }

        return next.handle(clonedRequest)
            .pipe(
                catchError(exception => {

                    const toastService = this.injector.get(ToastrService);
                    const translateService = this.injector.get(TranslateService);

                    // Not a http exception.
                    if (!(exception instanceof HttpErrorResponse)) {
                        return throwError(exception);
                    }

                    switch (exception.status) {
                        case 403:
                        case 404:
                        case 409:
                        case 500:

                            const httpException = exception.error as InvalidBusinessResponseViewModel;

                            if (!httpException || !httpException.code) {
                                return throwError(exception);
                            }

                            const translateKey = `INVALID_BUSINESS_EXCEPTION.${httpException.code}`;

                            const translatedMessage = translateService.instant(translateKey);
                            const translateTitle = translateService.instant('INVALID_BUSINESS_EXCEPTION.SYSTEM_ERROR');

                            toastService.error(translatedMessage, translateTitle);

                            return throwError(exception);
                    }

                    return throwError(exception);
                })
            );
    }

    //#endregion

}
