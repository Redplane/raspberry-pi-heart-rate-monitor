import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Inject, Injectable} from '@angular/core';
import {AUTHENTICATION_SERVICE_INJECTOR, NAVIGATION_SERVICE_PROVIDER} from '../constants/injection-token.constant';
import {Observable, of, throwError} from 'rxjs';
import {INgRxMessageBusService, MESSAGE_BUS_SERVICE_PROVIDER} from 'ngrx-message-bus';
import {catchError, flatMap, tap} from 'rxjs/operators';
import {IAuthenticationService} from '../services/interfaces/authentication-service.interface';
import {IUser} from '../interfaces/user.interface';
import {MessageChannelNameConstant} from '../constants/message-channel-name.constant';
import {MessageEventNameConstant} from '../constants/message-event-name.constant';
import {INavigationService} from '../services/interfaces/navigation-service.interface';
import {ScreenCodeConstant} from '../constants/screen-code.constant';
import {ProfileViewModel} from '../view-models/user/profile.view-model';

@Injectable()
export class ProfileResolve implements Resolve<ProfileViewModel> {

    //#region Constructor

    public constructor(@Inject(AUTHENTICATION_SERVICE_INJECTOR) protected authenticationService: IAuthenticationService,
                       @Inject(NAVIGATION_SERVICE_PROVIDER) protected navigationService: INavigationService,
                       @Inject(MESSAGE_BUS_SERVICE_PROVIDER) protected messageBusService: INgRxMessageBusService) {
    }

    //#endregion

    //#region Methods

    public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
        : Observable<ProfileViewModel> | Promise<ProfileViewModel> | ProfileViewModel {

        let loadedUser: IUser = null;

        return this.authenticationService
            .loadProfileAsync()
            .pipe(
                tap((user: ProfileViewModel) => {

                    loadedUser = user;
                    this.messageBusService
                        .addMessage<IUser>(MessageChannelNameConstant.ui,
                            MessageEventNameConstant.updateUserProfile, user);
                }),
                catchError(error => {
                    return this.authenticationService
                        .logOutAsync()
                        .pipe(
                            flatMap(_ => this.navigationService.navigateToScreenAsync(ScreenCodeConstant.login)),
                            flatMap(_ => throwError(error))
                        );
                })
            );
    }

    //#endregion
}
