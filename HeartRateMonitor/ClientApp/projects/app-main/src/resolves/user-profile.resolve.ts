import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Inject, Injectable} from '@angular/core';
import {
    AUTHENTICATION_SERVICE_INJECTOR,
    NAVIGATION_SERVICE_PROVIDER,
    USER_SERVICE_INJECTOR
} from '../constants/injection-token.constant';
import {IUserService} from '../services/interfaces/user-service.interface';
import {Observable, of, throwError} from 'rxjs';
import {INgRxMessageBusService, MESSAGE_BUS_SERVICE_PROVIDER} from 'ngrx-message-bus';
import {IAuthenticationService} from '../services/interfaces/authentication-service.interface';
import {UserViewModel} from '../view-models/user.view-model';
import {UserProfileRouteParam} from '../models/users/user-profile.route-param';
import {INavigationService} from '../services/interfaces/navigation-service.interface';
import {ScreenCodeConstant} from '../constants/screen-code.constant';
import {catchError, map} from 'rxjs/operators';

@Injectable()
export class UserProfileResolve implements Resolve<UserViewModel> {

    //#region Constructor

    public constructor(@Inject(USER_SERVICE_INJECTOR) protected userService: IUserService,
                       @Inject(AUTHENTICATION_SERVICE_INJECTOR) protected authenticationService: IAuthenticationService,
                       @Inject(MESSAGE_BUS_SERVICE_PROVIDER) protected messageBusService: INgRxMessageBusService,
                       @Inject(NAVIGATION_SERVICE_PROVIDER) protected navigationService: INavigationService) {
    }

    //#endregion

    //#region Methods

    public resolve(activatedRouteSnapshot: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<UserViewModel>
        | Promise<UserViewModel> | UserViewModel {

        const detailedUserParams: UserProfileRouteParam = activatedRouteSnapshot
            .params as UserProfileRouteParam;

        if (!detailedUserParams || !detailedUserParams.id) {
            // Emit a message-modal about the failed router resolve.
            return this.navigationService
                .navigateToScreenAsync(ScreenCodeConstant.users)
                .pipe(
                    map(_ => null)
                );
        }

        return this.userService
            .loadUserAsync(detailedUserParams.id)
            .pipe(
                catchError(_ => {
                    return this.navigationService
                        .navigateToScreenAsync(ScreenCodeConstant.users)
                        .pipe(
                            map(() => null)
                        );
                })
            );
    }

    //#endregion
}
