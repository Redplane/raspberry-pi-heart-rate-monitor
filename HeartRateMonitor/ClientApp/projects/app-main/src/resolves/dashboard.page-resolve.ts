import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {
    DASHBOARD_PAGE_SERVICE_PROVIDER,
    MEASUREMENT_PROCESS_SETTINGS_SERVICE_PROVIDER
} from '../constants/injection-token.constant';
import {IDashboardPageService} from '../services/interfaces/page-services/dashboard-page-service.interface';
import {IMeasurementProcessSettingsService} from '../services/interfaces/measurement-process-settings.service';
import {map, tap} from 'rxjs/operators';
import {DashboardPageService} from '../services/implementations/page-services/dashboard.page-service';

@Injectable()
export class DashboardPageResolve implements Resolve<void> {

    //#region Properties

    //#endregion

    //#region Constructor

    public constructor(@Inject(DASHBOARD_PAGE_SERVICE_PROVIDER) protected dashboardPageService: IDashboardPageService,
                       // tslint:disable-next-line:max-line-length
                       @Inject(MEASUREMENT_PROCESS_SETTINGS_SERVICE_PROVIDER) protected measurementProcessSettingsService: IMeasurementProcessSettingsService) {
    }

    //#endregion

    //#region Methods

    public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<void> | Promise<void> | void {
        return this.measurementProcessSettingsService
            .loadStatusAsync()
            .pipe(
                tap(measurementStatus => {
                    if (this.dashboardPageService instanceof DashboardPageService) {
                        const dashboardPageService = this.dashboardPageService as DashboardPageService;
                        dashboardPageService.setMeasurementProcessStatus(measurementStatus);
                    }
                }),
                map(() => void(0))
            );
    }

    //#endregion
}
