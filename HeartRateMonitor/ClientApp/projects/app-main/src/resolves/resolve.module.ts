import {NgModule} from '@angular/core';
import {ProfileResolve} from './profile.resolve';
import {RefreshTokenResolve} from './refresh-token.resolve';
import {UserProfileResolve} from './user-profile.resolve';
import {SerialPortConnectivityResolve} from './serial-port-connectivity.resolve';

@NgModule({
    providers: [
        ProfileResolve,
        RefreshTokenResolve,
        UserProfileResolve,
        SerialPortConnectivityResolve
    ]
})
export class ResolveModule {
}
