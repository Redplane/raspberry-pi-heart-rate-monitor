import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {Inject, Injectable} from '@angular/core';
import {AUTHENTICATION_SERVICE_INJECTOR} from '../constants/injection-token.constant';
import {IAuthenticationService} from '../services/interfaces/authentication-service.interface';
import {catchError, flatMap} from 'rxjs/operators';
import {AccessTokenRefreshingException} from '../models/refresh-tokens/access-token-refreshing-exception';
import {AccessTokenBeInFutureException} from '../models/refresh-tokens/access-token-be-in-future-exception';
import {LoginResultViewModel} from '../view-models/login-result.view-model';

@Injectable()
export class RefreshTokenResolve implements Resolve<any> {

    //#region Constructor

    public constructor(@Inject(AUTHENTICATION_SERVICE_INJECTOR) protected authenticationService: IAuthenticationService) {
    }

    //#endregion

    //#region Methods

    public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>
        | Promise<any> | any {

        // Try to refresh access token.
        return this.authenticationService
            .reloadAccessTokenAsync()
            .pipe(
                catchError(exception => {

                    // Another thread is refreshing access token.
                    if (exception instanceof AccessTokenRefreshingException) {
                        return of(null);
                    }

                    // Access token shouldn't be refreshed right now.
                    if (exception instanceof AccessTokenBeInFutureException) {
                        return this.authenticationService
                            .loadLoginResultAsync();
                    }

                    return of(null);
                }),
                flatMap((loginResult: LoginResultViewModel) => {

                    if (!loginResult) {
                        return of(null);
                    }

                    return this.authenticationService
                        .updateLoginResultAsync(loginResult);
                })
            );
    }

    //#endregion
}
