import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {Inject, Injectable} from '@angular/core';
import {INgRxMessageBusService, MESSAGE_BUS_SERVICE_PROVIDER} from 'ngrx-message-bus';

@Injectable()
export class SerialPortConnectivityResolve implements Resolve<boolean> {

    //#region Constructor

    public constructor(@Inject(MESSAGE_BUS_SERVICE_PROVIDER) protected messageBusService: INgRxMessageBusService) {
    }

    //#endregion

    //#region Methods

    public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        // return this.connectionService
        //     .getIsConnectedAsync()
        //     .pipe(
        //         tap(connected => {
        //             this.messageBusService.addMessage(MessageChannelNameConstant.connections,
        //                 MessageEventNameConstant.serialPortConnectionEstablished, connected);
        //         })
        //     )
        throw new Error('Not implemented exception');
    }

    //#endregion
}
