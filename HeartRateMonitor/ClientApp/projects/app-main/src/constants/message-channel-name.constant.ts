export class MessageChannelNameConstant {

    //#region Properties

    // Channel for broadcasting messages related to UI.
    public static readonly ui = 'UI';

    // Measurement.
    public static readonly measurement = 'measurement';

    //#endregion

}
