export class MessageEventNameConstant {

    //#region Properties

    public static readonly updateUserProfile = 'UPDATE_USER_PROFILE';

    public static readonly updateBreadcrumb = 'UPDATE_BREADCRUMB';

    public static readonly updatePageHeading = 'UPDATE_PAGE_HEADING';

    public static readonly didMeasurement = 'did-measurement';


    //#endregion

}
