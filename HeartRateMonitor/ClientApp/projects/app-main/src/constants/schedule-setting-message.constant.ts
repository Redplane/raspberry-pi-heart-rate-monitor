export class ScheduleSettingMessageConstant {

    public static readonly invalidScheduleSetting = 'INVALID_SCHEDULE_SETTING';

    public static readonly everyMinute = 'EVERY_X_MINUTE';

    public static readonly everyMinutes = 'EVERY_X_MINUTES';

    public static readonly fromMinuteToMinute = 'FROM_MINUTE_X_TO_MINUTE_Y';

    public static readonly atMinutes = 'AT_MINUTES';

    public static readonly everyHour = 'EVERY_X_HOUR';

    public static readonly everyHours = 'EVERY_X_HOURS';

    public static readonly fromHourToHour = 'FROM_HOUR_X_TO_HOUR_Y';

    public static readonly atHours = 'AT_HOURS';


}
