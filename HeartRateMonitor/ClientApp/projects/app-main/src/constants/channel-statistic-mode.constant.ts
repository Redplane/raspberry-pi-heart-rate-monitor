export class ChannelStatisticModeConstant {

    //#region Properties

    // Week statistic mode.
    public static readonly week = 'week';

    // Thirty day mode.
    public static readonly thirtyDays = 'month';

    //#endregion

}
