import {InjectionToken} from '@angular/core';
import {IAuthenticationService} from '../services/interfaces/authentication-service.interface';
import {IUserService} from '../services/interfaces/user-service.interface';
import {INavigationService} from '../services/interfaces/navigation-service.interface';
import {IIpcMessageHandler} from '../interfaces/ipc-message-handler.interface';
import {IPlatformService} from '../services/interfaces/platform-service.interface';
import {IDashboardPageService} from '../services/interfaces/page-services/dashboard-page-service.interface';
import {ISerialPortDetailDialogService} from '../services/interfaces/serial-port-detail-dialog-service.interface';
import {IEndPointResolver} from '../services/interfaces/endpoint.resolver';
import {ISerialPortService} from '../services/interfaces/serial-port-service.interface';
import {SerialPortSettings} from '@app/common';
import {IInOutPortService} from '../services/interfaces/in-out-port-service.interface';
import {IMeasurementProcessSettingsService} from '../services/interfaces/measurement-process-settings.service';
import {IMeasurementProcessSettingsDialogService} from '../services/interfaces/measurement-process-settings-dialog-service.interface';
import {MeasurementProcessSettingsViewModel} from '../view-models/measurement-process-settings.view-model';
import {IHttpExceptionMessageHandler} from '../services/interfaces/http-exception-messages/http-exception-message-handler.interface';

// Service injection tokens.
export const AUTHENTICATION_SERVICE_INJECTOR = new InjectionToken<IAuthenticationService>('AUTHENTICATION_SERVICE_INJECTOR');
export const USER_SERVICE_INJECTOR = new InjectionToken<IUserService>('USER_SERVICE_INJECTOR');
export const NAVIGATION_SERVICE_PROVIDER = new InjectionToken<INavigationService>('NAVIGATION_SERVICE_INJECTOR');
export const IPC_MESSAGE_HANDLER_PROVIDER = new InjectionToken<IIpcMessageHandler>('IPC_MESSAGE_HANDLER_PROVIDER');
export const PLATFORM_SERVICE_PROVIDER = new InjectionToken<IPlatformService>('PLATFORM_SERVICE_PROVIDER');
export const IN_OUT_PORT_SERVICE_PROVIDER = new InjectionToken<IInOutPortService>('IN_OUT_PORT_SERVICE_PROVIDER');
export const DASHBOARD_PAGE_SERVICE_PROVIDER = new InjectionToken<IDashboardPageService>('DASHBOARD_PAGE_SERVICE_PROVIDER');

// Serial port dialog.
export const SERIAL_PORT_DETAIL_DIALOG_PROVIDER = new InjectionToken<ISerialPortDetailDialogService>('SERIAL_PORT_DETAIL_DIALOG_PROVIDER');
export const SERIAL_PORT_SERVICE_PROVIDER = new InjectionToken<ISerialPortService>('SERIAL_PORT_SERVICE_PROVIDER');
export const INITIAL_SERIAL_PORT_PROVIDER = new InjectionToken<SerialPortSettings>('INITIAL_SERIAL_PORT_PROVIDER');
export const SERIAL_PORT_AVAILABLE_PORT_PROVIDER = new InjectionToken<string[]>('SERIAL_PORT_AVAILABLE_PORT_PROVIDER');

export const MEASUREMENT_PROCESS_SETTINGS_SERVICE_PROVIDER = new InjectionToken<IMeasurementProcessSettingsService>('MEASUREMENT_PROCESS_SETTINGS_SERVICE_PROVIDER');
export const MEASUREMENT_PROCESS_SETTINGS_PROVIDER = new InjectionToken<MeasurementProcessSettingsViewModel>('MEASUREMENT_PROCESS_SETTINGS_PROVIDER');
export const MEASUREMENT_PROCESS_SETTINGS_DIALOG_SERVICE_PROVIDER = new InjectionToken<IMeasurementProcessSettingsDialogService>('MEASUREMENT_PROCESS_SETTING_DIALOG_SERVICE_PROVIDER');

// Http exception message handler.
export const HTTP_EXCEPTION_MESSAGE_HANDLER = new InjectionToken<IHttpExceptionMessageHandler>('HTTP_EXCEPTION_MESSAGE_HANDLER');

// Endpoint resolver.
export const ENDPOINT_RESOLVER_PROVIDER = new InjectionToken<IEndPointResolver>('ENDPOINT_RESOLVER_PROVIDER');
