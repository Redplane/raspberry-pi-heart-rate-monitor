export class HttpMessageHandlerExceptionConstant {
  public static readonly skipped = 'skipped';

  public static readonly done = 'done';
}
