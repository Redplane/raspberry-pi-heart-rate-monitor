export class LocalStorageKeyConstant {

    //#region Properties

    public static loginResult = 'login-result';

    public static lastRefreshTokenAt = 'last-refresh-token-at';
  public static authenticatedYoutube = 'authenticated-youtube';

  //#endregion

    //#endregion

}
