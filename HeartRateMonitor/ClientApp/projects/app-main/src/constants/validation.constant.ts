export class ValidationConstant {

    //#region Properties

    // Default input max length.
    public static get defaultInputMaxLength() {
        return 255;
    }

    //#endregion

}
