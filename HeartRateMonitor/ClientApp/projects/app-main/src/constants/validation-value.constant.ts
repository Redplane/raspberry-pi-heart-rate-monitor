export class ValidationValueConstant {

    //#region Properties

    public static readonly minDeviceIndex = 0;

    public static readonly maxDeviceIndex = 255;

    public static readonly minDeviceInterval = 1;

    public static readonly minDeviceTimeout = -1;

    //#endregion

}
