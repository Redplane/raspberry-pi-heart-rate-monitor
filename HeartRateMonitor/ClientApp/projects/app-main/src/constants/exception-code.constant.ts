export class ExceptionCodeConstant {

    //#region Properties

    public static readonly accessTokenNotFound = 'ACCESS_TOKEN_NOT_FOUND';

    public static readonly refreshTokenNotFound = 'REFRESH_TOKEN_NOT_FOUND';

    public static readonly accessTokenIsRefreshing = 'ACCESS_TOKEN_IS_REFRESHING';

    public static readonly accessTokenWithinLifeTime = 'ACCESS_TOKEN_WITHIN_LIFE_TIME';

    public static readonly accessTokenShouldBeRefreshedInFuture = 'ACCESS_TOKEN_SHOULD_BE_REFRESHED_IN_FUTURE';

    public static readonly screenCodeNotFound = 'SCREEN_CODE_NOT_FOUND';

    public static readonly profileNotFound = 'PROFILE_NOT_FOUND';

    public static readonly platformIsNotWeb = 'PLATFORM_IS_NOT_WEB';

    public static readonly platformIsWeb = 'PLATFORM_IS_WEB';

    public static readonly itemNotFound = 'RECORD_NOT_FOUND';

    public static readonly dialogDismiss = 'DIALOG_DISMISS';

    //#endregion

}
