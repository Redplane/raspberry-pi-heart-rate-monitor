export class MeasurementFactors {

    //#region Properties

    public static readonly dustOne = 'PM1';

    public static readonly dustTwentyFive = 'PM2.5';

    public static readonly dustTen = 'PM10';

    public static readonly battery = 'BATT';

    //#endregion

}
