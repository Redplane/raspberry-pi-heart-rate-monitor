export class ScreenCodeConstant {

    //#region Properties

    public static login = 'LOGIN';

    public static users = 'USER_MANAGEMENT';

    public static profile = 'PROFILE';

    public static editMeasurementSnapshot = 'EDIT_MEASUREMENT_SNAPSHOT';

    public static addMeasurementSnapshot = 'ADD_MEASUREMENT_SNAPSHOT';

    public static measurementSnapshots = 'MEASUREMENT_SNAPSHOT_MANAGEMENT';

    public static dashboard = 'DASHBOARD';

    public static deviceMeasurement = 'DEVICE_MEASUREMENT';

    public static device = 'DEVICE';

    public static addDevice = 'ADD_DEVICE';

    public static editDevice = 'EDIT_DEVICE';

    public static addStation = 'ADD_STATION';

    public static editStation = 'EDIT_STATION';

    public static stationManagement = 'STATION_MANAGEMENT';

    //#endregion

}
