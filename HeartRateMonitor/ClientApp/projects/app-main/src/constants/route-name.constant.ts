export class RouteNameConstant {
    public static channels = '/channels';
    public static users = '/users';
    public static videos = '/videos';
    public static cms = '/cms';
    public static statistic = '/statistic';
    public static overallStatistic = '/statistic/overall';
    public static videoStatistic = '/statistic/video';
    public static weatherStation = '/sensor-measurements';
}
