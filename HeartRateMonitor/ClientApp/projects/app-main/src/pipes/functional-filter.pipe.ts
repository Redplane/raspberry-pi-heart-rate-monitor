import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'functionalFilter'
})
export class FunctionalFilterPipe implements PipeTransform {

    //#region Constructor

    public transform(values: any[], ...args: any[]): any {

        const items = values;
        const filter: (value: any) => any = args[0];

        if (!filter || !(filter instanceof Function)) {
            return items || [];
        }
        return filter(items) || [];
    }

    //#endregion
}