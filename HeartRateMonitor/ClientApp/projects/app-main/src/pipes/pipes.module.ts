import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ToTrustedUrlPipe} from './to-trusted-url.pipe';
import {ToBase64Pipe} from './to-base64.pipe';
import {StatusNotInCollectionPipe} from './status-not-in-collection.pipe';
import {FunctionalFilterPipe} from './functional-filter.pipe';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule
    ],
    declarations: [
        ToTrustedUrlPipe,
        ToBase64Pipe,
        StatusNotInCollectionPipe,
        FunctionalFilterPipe
    ],
    providers: [
        ToTrustedUrlPipe,
        ToBase64Pipe,
        StatusNotInCollectionPipe,
        FunctionalFilterPipe
    ],
    exports: [
        ToTrustedUrlPipe,
        ToBase64Pipe,
        StatusNotInCollectionPipe,
        FunctionalFilterPipe
    ]
})
export class PipesModule {
}
