import { Pipe } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({ name: 'toTrustedUrl' })
export class ToTrustedUrlPipe {

  constructor(private sanitizer: DomSanitizer) {
  }

  transform(style, blankImageUrl: string) {
    if (!style && blankImageUrl) {
      return this.sanitizer.bypassSecurityTrustUrl(blankImageUrl);
    }
    return this.sanitizer.bypassSecurityTrustUrl(style);
  }
}
