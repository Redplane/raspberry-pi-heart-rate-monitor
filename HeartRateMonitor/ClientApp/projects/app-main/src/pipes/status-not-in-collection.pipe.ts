import {Pipe, PipeTransform} from '@angular/core';
import {VideoStatusViewModel} from '../view-models/video-status.view-model';

@Pipe({
    name: 'statusNotInCollection'
})
export class StatusNotInCollectionPipe implements PipeTransform {


    //#region Methods

    public transform(items: VideoStatusViewModel[], status: string): any {
        if (!items || !status) {
            return items;
        }

        return items.filter(item => item.id !== status);
    }

    //#endregion
}
