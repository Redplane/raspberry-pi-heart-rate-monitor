import {Pipe, PipeTransform} from '@angular/core';
import {Observable, from} from 'rxjs';
import {map} from 'rxjs/operators';

@Pipe({
  name: 'toBase64'
})
export class ToBase64Pipe implements PipeTransform {

  //#region Methods

  /*
  * Convert data source to base64 string asynchronously.
  * */
  public transform(value: any, args?: any): Observable<string> {

    const loadEncodedDataPromise = new Promise(resolve => {
      const fileReader = new FileReader();
      fileReader.readAsDataURL(value);
      fileReader.onloadend = () => {
        resolve(fileReader.result);
      };
    });

    return from(loadEncodedDataPromise)
      .pipe(
        map((encodedData: string) => {
          return encodedData as string;
        }));
  }

  //#endregion
}
