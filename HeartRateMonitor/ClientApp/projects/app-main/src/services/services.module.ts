import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {
    AUTHENTICATION_SERVICE_INJECTOR,
    DASHBOARD_PAGE_SERVICE_PROVIDER,
    ENDPOINT_RESOLVER_PROVIDER,
    IN_OUT_PORT_SERVICE_PROVIDER,
    IPC_MESSAGE_HANDLER_PROVIDER, MEASUREMENT_PROCESS_SETTINGS_SERVICE_PROVIDER,
    NAVIGATION_SERVICE_PROVIDER,
    PLATFORM_SERVICE_PROVIDER,
    SERIAL_PORT_SERVICE_PROVIDER,
    USER_SERVICE_INJECTOR
} from '../constants/injection-token.constant';
import {AuthenticationService} from './implementations/authentication.service';
import {NavigationService} from './implementations/navigation.service';
import {UserService} from './implementations/user.service';
import {PlatformService} from './implementations/platform.service';
import {MeasurementDoneIpcHandler} from './implementations/ipc-message-handlers/measurement-done.ipc-handler';
import {DashboardPageService} from './implementations/page-services/dashboard.page-service';
import {SerialPortService} from './implementations/serial-port.service';
import {AppEndpointResolver} from './implementations/app-endpoint.resolver';
import {InOutPortService} from './implementations/in-out-port.service';
import {MeasurementProcessSettingsService} from './implementations/measurement-process-settings.service';

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule
    ],
    providers: [
        {
            provide: USER_SERVICE_INJECTOR,
            useClass: UserService
        },
        {
            provide: AUTHENTICATION_SERVICE_INJECTOR,
            useClass: AuthenticationService
        },
        {
            provide: NAVIGATION_SERVICE_PROVIDER,
            useClass: NavigationService
        },
        {
            provide: IPC_MESSAGE_HANDLER_PROVIDER,
            useClass: MeasurementDoneIpcHandler,
            multi: true
        },
        {
            provide: PLATFORM_SERVICE_PROVIDER,
            useClass: PlatformService
        },
        {
            provide: SERIAL_PORT_SERVICE_PROVIDER,
            useClass: SerialPortService
        },
        {
            provide: ENDPOINT_RESOLVER_PROVIDER,
            useClass: AppEndpointResolver
        },
        {
            provide: IN_OUT_PORT_SERVICE_PROVIDER,
            useClass: InOutPortService
        },
        {
            provide: MEASUREMENT_PROCESS_SETTINGS_SERVICE_PROVIDER,
            useClass: MeasurementProcessSettingsService
        }
    ],
    entryComponents: []
})
export class ServicesModule {
}
