import {Observable} from 'rxjs';
import {LoadUsersViewModel} from '../../view-models/load-users.view-model';
import {UserViewModel} from '../../view-models/user.view-model';
import {SearchResultViewModel} from '../../view-models/search-result.view-model';

export interface IUserService {

    //#region Methods

    loadUsersAsync(model: LoadUsersViewModel): Observable<SearchResultViewModel<UserViewModel>>;

    // Load user by using id asynchronously.
    loadUserAsync(id: string): Observable<UserViewModel>;

    loadPhotoUrlAsync(photo: string): Observable<string>;

    loadUsersByIdsAsync(ids: string[]): Observable<UserViewModel[]>;
    //#endregion
}
