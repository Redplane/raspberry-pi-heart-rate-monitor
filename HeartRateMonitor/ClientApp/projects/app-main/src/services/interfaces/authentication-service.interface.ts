import {Observable} from 'rxjs';
import {LoginResultViewModel} from '../../view-models/login-result.view-model';
import {LoginViewModel} from '../../view-models/login.view-model';
import {UserViewModel} from '../../view-models/user.view-model';
import {ProfileViewModel} from '../../view-models/user/profile.view-model';
import {HttpRequest} from '@angular/common/http';

export interface IAuthenticationService {

    //#region Methods

    // Clean up user information to log user out of system.
    logOutAsync(): Observable<void>;

    // Check whether user has access token or not.
    hasAccessTokenAsync(): Observable<boolean>;

    // Load user profile asynchronously.
    loadProfileAsync(): Observable<ProfileViewModel>;

    // Install a timer to refresh the access token.
    addRefreshTokenTimer(): void;

    // Uninstall the timer to refresh the access token.
    uninstallRefreshTokenTimer(): void;

    // Reload access token asynchronously.
    reloadAccessTokenAsync(): Observable<LoginResultViewModel>;

    // Get the login result stored in the system.
    loadLoginResultAsync(): Observable<LoginResultViewModel>;

    // Update login result into system asynchronously.
    updateLoginResultAsync(loginResult: LoginResultViewModel): Observable<void>;

    // Is request is logout url.
    isLogoutRequestAsync(httpRequest: HttpRequest<any>): Observable<boolean>;

    //#endregion

}
