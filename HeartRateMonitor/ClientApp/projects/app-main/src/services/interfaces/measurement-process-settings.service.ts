import {Observable} from 'rxjs';
import {MeasurementProcessSettingsViewModel} from '../../view-models/measurement-process-settings.view-model';
import {UpdateMeasurementProcessSettingsViewModel} from '../../view-models/update-measurement-process-settings.view-model';
import {MeasurementProcessStatuses} from '../../enums/measurement-process-statuses';

export interface IMeasurementProcessSettingsService {

    //#region Properties

    loadSettingsAsync(): Observable<MeasurementProcessSettingsViewModel>;

    editSettingsAsync(command: UpdateMeasurementProcessSettingsViewModel): Observable<MeasurementProcessSettingsViewModel>;

    loadStatusAsync(): Observable<MeasurementProcessStatuses>;

    startAsync(): Observable<void>;

    stopAsync(): Observable<void>;

    //#endregion

}
