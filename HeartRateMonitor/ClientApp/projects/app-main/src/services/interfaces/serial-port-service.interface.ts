import {Observable} from 'rxjs';
import {SelectableItem} from '../../models/selectable-item';
import {AddSerialPortViewModel} from '../../view-models/connection/add-serial-port.view-model';
import {EditSerialPortViewModel} from '../../view-models/connection/edit-serial-port.view-model';

export interface ISerialPortService {

    //#region Methods

    // Get list of available baud rates the serial port supports.
    getAvailableBaudRatesAsync(): Observable<number[]>;

    // Get available parities asynchronously.
    getAvailableParitiesAsync(): Observable<SelectableItem<number>[]>;

    // Get available stop bits.
    getAvailableStopBitsAsync(): Observable<SelectableItem<number>[]>;

    // Check whether connections is connected or not.
    getIsConnectedAsync(connectionId: string): Observable<boolean>;

    // Get available serial ports asynchronously.
    getAvailablePortsAsync(): Observable<string[]>;

    //#endregion

}
