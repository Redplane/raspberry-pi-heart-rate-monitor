import {Observable} from 'rxjs';

export interface IEndPointResolver {

  //#region Properties

  // Get end point about a module.
  loadEndPointAsync(moduleName: string,
                    functionName?: string): Observable<string>;

  //#endregion

}
