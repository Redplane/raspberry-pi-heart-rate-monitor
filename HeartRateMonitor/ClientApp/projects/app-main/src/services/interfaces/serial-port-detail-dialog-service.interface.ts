import {Observable} from 'rxjs';
import {AddSerialPortViewModel} from '../../view-models/connection/add-serial-port.view-model';
import {EditSerialPortViewModel} from '../../view-models/connection/edit-serial-port.view-model';
import {SerialPortSettings} from '@app/common';

export interface ISerialPortDetailDialogService {

    //#region Methods

    displayAddSerialPortDialogAsync(): Observable<AddSerialPortViewModel>;

    displayEditSerialPortDialogAsync(settings: SerialPortSettings): Observable<EditSerialPortViewModel>;

    //#endregion

}
