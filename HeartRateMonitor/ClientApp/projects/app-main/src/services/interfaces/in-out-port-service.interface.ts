import {Observable} from 'rxjs';
import {SerialPortSettings} from '@app/common';
import {SetupInOutPortViewModel} from '../../view-models/in-out-ports/setup-in-out-port.view-model';

export interface IInOutPortService {

    //#region Methods

    getSettingsAsync(): Observable<SerialPortSettings>;

    setupAsync(model: SetupInOutPortViewModel): Observable<SerialPortSettings>;

    //#endregion

}
