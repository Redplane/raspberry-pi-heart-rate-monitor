import {Observable} from 'rxjs';
import {MeasurementDataChart} from '../../../models/measurements/measurement-data-chart';
import {MeasurementProcessStatuses} from '../../../enums/measurement-process-statuses';

export interface IDashboardPageService {

    //#region Methods

    // Load measurement chart asynchronously.
    loadMeasurementChartAsync(): Observable<{ [name: string]: MeasurementDataChart }>;

    // Load measurement status.
    loadMeasurementProcessStatus(): MeasurementProcessStatuses;

    //#endregion

}
