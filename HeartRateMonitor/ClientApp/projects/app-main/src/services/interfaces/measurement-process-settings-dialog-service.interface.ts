import {Observable} from 'rxjs';
import {UpdateMeasurementProcessSettingsViewModel} from '../../view-models/update-measurement-process-settings.view-model';
import {MeasurementProcessSettingsViewModel} from '../../view-models/measurement-process-settings.view-model';

export interface IMeasurementProcessSettingsDialogService {

    //#region Methods

    displayAsync(initialSettings: MeasurementProcessSettingsViewModel): Observable<UpdateMeasurementProcessSettingsViewModel>;

    //#endregion

}
