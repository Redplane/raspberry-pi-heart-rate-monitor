import {Observable} from 'rxjs';
import {Platforms} from '../../enums/platforms.enum';

export interface IPlatformService {

    //#region Properties

    // Get platform the application runs on.
    getPlatformAsync(): Observable<Platforms>;

    //#endregion

}
