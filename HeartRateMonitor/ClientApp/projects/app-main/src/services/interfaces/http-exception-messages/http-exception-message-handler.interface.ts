import {HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {HttpMessageHandleResults} from '../../../enums/http-message-handle-results';

export interface IHttpExceptionMessageHandler {

  //#region Methods

  // Handle http exception message asynchronously.
  handleAsync(exception: HttpErrorResponse): Observable<HttpMessageHandleResults>;

  //#endregion

}
