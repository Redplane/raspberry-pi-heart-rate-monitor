import {ISerialPortDetailDialogService} from '../interfaces/serial-port-detail-dialog-service.interface';
import {Inject, Injectable, Injector} from '@angular/core';
import {from, Observable, of, throwError} from 'rxjs';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {SerialPortDetailDialogComponent} from '../../modules/shared/serial-port-detail-dialog/serial-port-detail-dialog.component';
import {AddSerialPortViewModel} from '../../view-models/connection/add-serial-port.view-model';
import {EditSerialPortViewModel} from '../../view-models/connection/edit-serial-port.view-model';
import {SerialPortSettings} from '@app/common';
import {
    INITIAL_SERIAL_PORT_PROVIDER,
    SERIAL_PORT_AVAILABLE_PORT_PROVIDER,
    SERIAL_PORT_SERVICE_PROVIDER
} from '../../constants/injection-token.constant';
import {ISerialPortService} from '../interfaces/serial-port-service.interface';
import {catchError, mergeMap} from 'rxjs/operators';
import {ExceptionCodeConstant} from '../../constants/exception-code.constant';

@Injectable()
export class SerialPortDetailDialogService implements ISerialPortDetailDialogService {

    //#region Properties

    protected modalService: NgbModal;

    protected serialPortService: ISerialPortService;

    //#endregion

    //#region Constructor

    public constructor(protected injector: Injector) {
        this.modalService = injector.get(NgbModal);
        this.serialPortService = injector.get(SERIAL_PORT_SERVICE_PROVIDER);
    }

    //#endregion


    //#region Methods

    public displayAddSerialPortDialogAsync(): Observable<AddSerialPortViewModel> {

        return this.serialPortService
            .getAvailablePortsAsync()
            .pipe(
                mergeMap(availablePorts => {

                    const childInjector = Injector.create({
                        providers: [
                            {
                                provide: SERIAL_PORT_AVAILABLE_PORT_PROVIDER,
                                useValue: availablePorts
                            }
                        ],
                        parent: this.injector
                    });

                    const addSerialPortDialogRef = this.modalService
                        .open(SerialPortDetailDialogComponent, {
                            backdrop: true.valueOf(),
                            injector: childInjector
                        });

                    const dialogResult = addSerialPortDialogRef.result;
                    debugger;
                    return from(dialogResult)
                        .pipe(
                            catchError(_ => {
                               return  throwError(ExceptionCodeConstant.dialogDismiss);
                            })
                        );
                })
            );
    }

    public displayEditSerialPortDialogAsync(settings: SerialPortSettings): Observable<EditSerialPortViewModel> {

        return this.serialPortService
            .getAvailablePortsAsync()
            .pipe(
                mergeMap(availablePorts => {
                    const injector = Injector.create({
                        providers: [
                            {
                                provide: INITIAL_SERIAL_PORT_PROVIDER,
                                useValue: settings
                            },
                            {
                                provide: SERIAL_PORT_AVAILABLE_PORT_PROVIDER,
                                useValue: availablePorts
                            }
                        ]
                    });

                    const serialPortDetailDialog = this.modalService
                        .open(SerialPortDetailDialogComponent, {
                            backdrop: true,
                            injector
                        });

                    return from(serialPortDetailDialog.result)
                        .pipe(
                            catchError(_ => throwError(ExceptionCodeConstant.dialogDismiss))
                        );
                })
            );
    }

    //#endregion

}
