import {IAuthenticationService} from '../interfaces/authentication-service.interface';
import {Injectable} from '@angular/core';
import {forkJoin, from, Observable, of, Subscription, throwError, timer} from 'rxjs';
import {LoginResultViewModel} from '../../view-models/login-result.view-model';
import {AppSettingsService} from './app-settings.service';
import {
    catchError, delay,
    delayWhen,
    flatMap,
    map, repeat,
    retryWhen,
    switchMap, switchMapTo,
    tap,
} from 'rxjs/operators';
import {HttpClient, HttpEvent, HttpHeaders, HttpParams, HttpRequest} from '@angular/common/http';
import {LocalStorageKeyConstant} from '../../constants/local-storage-key.constant';
import {StorageMap} from '@ngx-pwa/local-storage';
import {Router} from '@angular/router';
import {UserViewModel} from '../../view-models/user.view-model';
import {ExceptionCodeConstant} from '../../constants/exception-code.constant';
import {ProfileViewModel} from '../../view-models/user/profile.view-model';
import {cloneDeep} from 'lodash';
import {UserService} from './user.service';
import {AppSettings} from '../../models/app-settings.model';
import {AccessTokenRefreshingException} from '../../models/refresh-tokens/access-token-refreshing-exception';
import {AccessTokenBeInFutureException} from '../../models/refresh-tokens/access-token-be-in-future-exception';
import {merge as lodashMerge} from 'lodash';
import {RefreshTokenViewModel} from '../../view-models/user/refresh-token.view-model';

// Access token life time.
// export const ACCESS_TOKEN_LIFE_TIME = 60 * 1000; //3600000;
// export const REFRESH_TIME_PERCENTAGE = 0.8;
// export const REFRESH_TIME_RETRY_GAP = ACCESS_TOKEN_LIFE_TIME * (1.0 - REFRESH_TIME_PERCENTAGE) / 100 / 3;
// export const REFRESH_TIME_GAP = 30 * 1000; //30000;
// export const REFRESH_RETRIES = 3;
export const ACCESS_TOKEN_LIFE_TIME = 1800000;
export const REFRESH_TIME_PERCENTAGE = 0.8;
export const REFRESH_TIME_RETRY_GAP = ACCESS_TOKEN_LIFE_TIME * (1.0 - REFRESH_TIME_PERCENTAGE) / 100 / 3;
export const REFRESH_TIME_GAP = 30000;
export const REFRESH_RETRIES = 3;

@Injectable()
export class AuthenticationService extends UserService implements IAuthenticationService {

    //#region Properties

    // Subscription about refresh token handle.
    // tslint:disable-next-line:variable-name
    private _handleRefreshTokenSubscription: Subscription;

    //#endregion

    //#region Constructor

    public constructor(protected appSettingsService: AppSettingsService,
                       protected storageMap: StorageMap,
                       protected router: Router,
                       protected httpClient: HttpClient) {
        super(appSettingsService, httpClient);
    }

    //#endregion

    //#region Methods

    // Get last time the access token was refreshed.
    public loadLastRefreshTokenTimeAsync(): Observable<number> {
        return this.storageMap
            .get<number>(LocalStorageKeyConstant.lastRefreshTokenAt)
            .pipe(
                map(time => time as number)
            );
    }

    // Get the login result stored in the system.
    public loadLoginResultAsync(): Observable<LoginResultViewModel> {
        return this.storageMap
            .get<LoginResultViewModel>(LocalStorageKeyConstant.loginResult)
            .pipe(
                map((loginResult: LoginResultViewModel) => {
                    if (!loginResult || !loginResult.accessToken || !loginResult.accessToken.trim()) {
                        throw new Error(ExceptionCodeConstant.accessTokenNotFound);
                    }

                    if (!loginResult.refreshToken || !loginResult.refreshToken.trim()) {
                        throw new Error(ExceptionCodeConstant.refreshTokenNotFound);
                    }

                    return loginResult;
                })
            );
    }

    // Load refresh token asynchronously.
    public reloadAccessTokenAsync(): Observable<LoginResultViewModel> {

        let accessToken = '';
        let refreshToken = '';
        let lastRefreshTokenTime: number;
        let issuedTokenTime: number;
        let accessTokenLifeTime = ACCESS_TOKEN_LIFE_TIME;
        let user: ProfileViewModel;

        // Load the last time the application refresh the access token.
        const loadLastRefreshTokenTimeObservable = this
            .loadLastRefreshTokenTimeAsync()
            .pipe(
                tap(time => lastRefreshTokenTime = time)
            );

        // Load login result from storage map observable.
        const loadLoginResultObservable = this.loadLoginResultAsync()
            .pipe(
                tap((loginResult: LoginResultViewModel) => {
                    accessToken = loginResult.accessToken;
                    refreshToken = loginResult.refreshToken;
                    issuedTokenTime = loginResult.issuedAt;
                    accessTokenLifeTime = loginResult.lifeTime;
                    user = loginResult.user;
                })
            );

        return forkJoin([
            loadLastRefreshTokenTimeObservable,
            loadLoginResultObservable])
            .pipe(
                flatMap(_ => {

                    // Get the current time in the system.
                    const currentTime = new Date().getTime();

                    // Calculate the time to refresh the access token.
                    const timeToRefreshToken = this.calculateAccessTokenRefreshTime(issuedTokenTime,
                        ACCESS_TOKEN_LIFE_TIME);

                    // Access token hasn't been refreshed before.
                    if (lastRefreshTokenTime === undefined || lastRefreshTokenTime === null) {
                        // Refresh time was in the past. Refresh the token now.
                        if (currentTime >= timeToRefreshToken) {
                            console.log(`Current time (${currentTime}) >= timeToRefreshToken (${timeToRefreshToken})`);
                            return this.loadRefreshTokenAsync(refreshToken);
                        }
                    }

                    // Access token has refreshed before, however, another thread is handling the operation.
                    // Retry after the specific time.
                    if (currentTime - lastRefreshTokenTime < REFRESH_TIME_GAP) {
                        console.log(`Current time (${currentTime}) - lastRefreshTokenTime (${lastRefreshTokenTime}) < ${REFRESH_TIME_GAP}`);
                        return throwError(new AccessTokenRefreshingException());
                    }

                    // Refresh time is in the future.
                    if (currentTime < timeToRefreshToken) {
                        console.log(`Current time (${currentTime}) < timeToRefreshToken (${timeToRefreshToken})`);
                        const delayTime = timeToRefreshToken - currentTime;
                        return throwError(new AccessTokenBeInFutureException(delayTime));
                    }

                    return this.loadRefreshTokenAsync(refreshToken);
                })
            );

    }

    // Add a refresh token timer.
    public buildRefreshTokenTimerAsync(): Observable<LoginResultViewModel> {

        let retries = 0;
        let oldLoginResult: LoginResultViewModel;

        return this.reloadAccessTokenAsync()
            .pipe(
                flatMap((loginResult: LoginResultViewModel) => {

                    // Reset the retries time.
                    retries = 0;
                    oldLoginResult = loginResult;

                    return this.storageMap
                        .set(LocalStorageKeyConstant.loginResult, loginResult)
                        .pipe(
                            flatMap(_ => {
                                return this.storageMap.set(LocalStorageKeyConstant.lastRefreshTokenAt, new Date().getTime());
                            }),
                            map(() => {
                                return loginResult;
                            })
                        );
                }),
                repeat(),
                retryWhen(errors =>
                    errors
                        .pipe(
                            delayWhen(exception => {

                                // Increase the retry time.
                                retries++;
                                if (retries > REFRESH_RETRIES) {
                                    return throwError(exception);
                                }

                                // Another thread is refreshing access token.
                                if (exception instanceof AccessTokenRefreshingException) {
                                    return timer(REFRESH_TIME_GAP);
                                }

                                // Refresh token should be in future.
                                if (exception instanceof AccessTokenBeInFutureException) {
                                    return timer((exception as AccessTokenBeInFutureException).timeToRefresh);
                                }

                                if (exception instanceof Error) {

                                    const actualError = exception as Error;

                                    // No access token is found.
                                    if (actualError.message === ExceptionCodeConstant.accessTokenNotFound) {
                                        return throwError(exception);
                                    }

                                    if (actualError.message === ExceptionCodeConstant.refreshTokenNotFound) {
                                        throw throwError(exception);
                                    }
                                }
                                // Get the current time in the system.
                                const currentTime = new Date().getTime();

                                if (!oldLoginResult) {
                                    return throwError(exception);
                                }

                                // Calculate the time to refresh the access token.
                                const timeToRefreshToken = this.calculateAccessTokenRefreshTime(oldLoginResult.issuedAt,
                                    ACCESS_TOKEN_LIFE_TIME);
                                if (currentTime >= timeToRefreshToken) {
                                    return timer(REFRESH_TIME_RETRY_GAP);
                                }

                                return throwError(exception);
                            })
                        )
                )
            );
    }

    // Install timer to exchange refresh token with a new access token.
    public addRefreshTokenTimer(): void {

        // Close the previous subscription.
        if (this._handleRefreshTokenSubscription && !this._handleRefreshTokenSubscription.closed) {
            this._handleRefreshTokenSubscription.unsubscribe();
        }

        this._handleRefreshTokenSubscription = this
            .buildRefreshTokenTimerAsync()
            .subscribe();
    }

    // Clean up user information and log user out of system.
    public logOutAsync(): Observable<void> {

        return this.appSettingsService
            .getAppSettingsAsync()
            .pipe(
                switchMap((appSettings: AppSettings) => {
                    const fullUrl = `${appSettings.baseUrl}/api/auth/logout`;

                    const logOutObservable = this.httpClient
                        .put(fullUrl, {})
                        .pipe(
                            // Suppress the exception.
                            catchError(error => of(null))
                        );

                    const deleteStorageObservable = this.storageMap
                        .delete(LocalStorageKeyConstant.loginResult);

                    return forkJoin([logOutObservable, deleteStorageObservable]);
                }),
                map(() => void (0))
            );
    }

    // Is request is logout url.
    public isLogoutRequestAsync(httpRequest: HttpRequest<any>): Observable<boolean> {
        return this.appSettingsService
            .getAppSettingsAsync()
            .pipe(
                map((appSettings: AppSettings) => {
                    const fullUrl = `${appSettings.baseUrl}/api/auth/logout`;
                    return httpRequest.url === fullUrl;
                })
            );
    }

    /* Use to request new token*/
    public loadRefreshTokenAsync(refreshToken: string): Observable<LoginResultViewModel> {

        return this.loadLoginResultAsync()
            .pipe(
                switchMap(oldLoginResult => {
                    return this.appSettingsService
                        .getAppSettingsAsync()
                        .pipe(
                            flatMap((appSettings: AppSettings) => {
                                const fullUrl = `${appSettings.baseUrl}/api/auth/token/refresh`;

                                return this.httpClient
                                    .post<RefreshTokenViewModel>(fullUrl, {refreshToken});
                            }),
                            map((refreshTokenResult: RefreshTokenViewModel) => {

                                const loginResult = new LoginResultViewModel();

                                return lodashMerge(loginResult, oldLoginResult, {
                                    lifeTime: ACCESS_TOKEN_LIFE_TIME,
                                    issuedAt: new Date().getTime(),
                                    refreshToken,
                                    accessToken: refreshTokenResult.accessToken
                                });
                            })
                        );
                })
            );

    }

    // Uninstall the refresh token timer.
    public uninstallRefreshTokenTimer(): void {

        if (this._handleRefreshTokenSubscription && !this._handleRefreshTokenSubscription.closed) {
            this._handleRefreshTokenSubscription.unsubscribe();
        }
    }

    // Check whether user has access token or not.
    public hasAccessTokenAsync(): Observable<boolean> {
        return this.storageMap
            .get(LocalStorageKeyConstant.loginResult)
            .pipe(
                map((loginResult: LoginResultViewModel) => {
                    if (!loginResult || !loginResult.accessToken || !loginResult.accessToken.trim().length) {
                        return false;
                    }

                    if (!loginResult.user) {
                        return false;
                    }

                    return true;
                })
            );
    }

    // Load profile asynchronously
    public loadProfileAsync(): Observable<ProfileViewModel> {
        // Get the login result.
        return this.storageMap
            .get(LocalStorageKeyConstant.loginResult)
            .pipe(
                map((loginResult: LoginResultViewModel) => {
                    if (!loginResult || !loginResult.accessToken || !loginResult.accessToken.trim().length) {
                        throw new Error(ExceptionCodeConstant.profileNotFound);
                    }

                    if (!loginResult.user) {
                        throw new Error(ExceptionCodeConstant.profileNotFound);
                    }

                    return loginResult.user.id;
                }),
                flatMap((id: string) => {
                    return this.loadUserAsync(id)
                        .pipe(
                            map((user: UserViewModel) => {
                                const profile = cloneDeep(user) as any as ProfileViewModel;
                                return profile;
                            })
                        );
                })
            );
    }

    // Calculate time to refresh access token.
    public calculateAccessTokenRefreshTime(issuedTime: number, lifeTime: number): number {
        return issuedTime + REFRESH_TIME_PERCENTAGE * lifeTime;
    }

    // Update login result into storage asynchronously.
    public updateLoginResultAsync(loginResult: LoginResultViewModel): Observable<void> {

        return this.storageMap
            .get(LocalStorageKeyConstant.loginResult)
            .pipe(
                flatMap((oldLoginResult: LoginResultViewModel) => {
                    return this.storageMap
                        .set(LocalStorageKeyConstant.loginResult, lodashMerge(oldLoginResult, loginResult));
                })
            );

    }

    //#endregion
}
