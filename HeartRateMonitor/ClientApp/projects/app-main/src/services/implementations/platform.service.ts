import {IPlatformService} from '../interfaces/platform-service.interface';
import {Inject, Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Platforms} from '../../enums/platforms.enum';
import {WINDOW} from './window.service';
import {AppSettingsService} from './app-settings.service';
import {map, mergeMap, tap} from 'rxjs/operators';
import {AppSettings} from '../../models/app-settings.model';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class PlatformService implements IPlatformService {

    //#region Constructor

    public constructor(@Inject(WINDOW) protected windowService: Window,
                       protected httpClient: HttpClient,
                       protected appSettingsService: AppSettingsService) {
    }

    //#endregion

    //#region Methods

    public getPlatformAsync(): Observable<Platforms> {

        // Cef query/
        const cefQuery = (this.windowService as any).cefQuery;

        // Cef query is not defined. That means the application is hosted on web.
        if (!cefQuery) {
            return of(Platforms.web);
        }

        return this.appSettingsService
            .getAppSettingsAsync()
            .pipe(
                mergeMap((appSettings: AppSettings) => {

                    const fullUrl = `${appSettings.baseUrl}/system/platform`;
                    return this.httpClient.get<Platforms>(fullUrl);
                })
            );
    }

    //#endregion
}
