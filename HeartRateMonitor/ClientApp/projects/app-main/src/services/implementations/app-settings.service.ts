import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {forkJoin, Observable, of} from 'rxjs';
import {catchError, map, mergeMap, tap} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {AppSettings} from '../../models/app-settings.model';

@Injectable()
export class AppSettingsService {

    //#region Properties

    // tslint:disable-next-line:variable-name
    private _appSettings: AppSettings;

    //#endregion

    //#region Constructors

    public constructor(protected httpClient: HttpClient) {

    }

    //#endregion

    //#region Application configuration

    /*
    * Load app configuration from json file.
    * */
    public getAppSettingsAsync(): Observable<AppSettings> {

        if (this._appSettings) {
            return of(this._appSettings);
        }

        const loadInitialOptionsFileObservable = this.httpClient
            .get<AppSettings>('/assets/appsettings.json');

        const loadEnvironmentalOptionsFileObservable = this.httpClient
            .get<AppSettings>(`/assets/appsettings.${environment.name}.json`)
            .pipe(
                catchError(error => of({}))
            );

        return forkJoin([loadInitialOptionsFileObservable,
            loadEnvironmentalOptionsFileObservable])
            .pipe(
                mergeMap((settings: any[]) => {
                    let appSettings = {} as AppSettings;
                    for (const setting of settings) {
                        appSettings = Object.assign(appSettings, setting);
                    }

                    this._appSettings = appSettings;
                    return of(appSettings);
                })
            );

    }

    public loadStoredConfiguration(): AppSettings {
        return this._appSettings;
    }

    //#endregion
}
