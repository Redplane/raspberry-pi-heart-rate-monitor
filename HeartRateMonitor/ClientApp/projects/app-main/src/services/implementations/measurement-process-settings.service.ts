import {IMeasurementProcessSettingsService} from '../interfaces/measurement-process-settings.service';
import {Injectable, Injector} from '@angular/core';
import {Observable} from 'rxjs';
import {MeasurementProcessSettingsViewModel} from '../../view-models/measurement-process-settings.view-model';
import {HttpClient} from '@angular/common/http';
import {IEndPointResolver} from '../interfaces/endpoint.resolver';
import {ENDPOINT_RESOLVER_PROVIDER} from '../../constants/injection-token.constant';
import {mergeMap} from 'rxjs/operators';
import {UpdateMeasurementProcessSettingsViewModel} from '../../view-models/update-measurement-process-settings.view-model';
import {MeasurementProcessStatuses} from '../../enums/measurement-process-statuses';

@Injectable()
export class MeasurementProcessSettingsService implements IMeasurementProcessSettingsService {

    //#region Properties

    // tslint:disable-next-line:variable-name
    protected readonly _httpClient: HttpClient;

    // tslint:disable-next-line:variable-name
    protected readonly _endPointResolver: IEndPointResolver;

    //#endregion

    //#region Constructor

    public constructor(injector: Injector) {
        this._httpClient = injector.get(HttpClient);
        this._endPointResolver = injector.get(ENDPOINT_RESOLVER_PROVIDER);
    }

    //#endregion

    //#region Methods

    public loadSettingsAsync(): Observable<MeasurementProcessSettingsViewModel> {
        return this._endPointResolver
            .loadEndPointAsync(null, null)
            .pipe(
                mergeMap(baseUrl => {
                    const fullUrl = `${baseUrl}/api/measurement-process`;
                    return this._httpClient.get<MeasurementProcessSettingsViewModel>(fullUrl);
                })
            );
    }

    public editSettingsAsync(command: UpdateMeasurementProcessSettingsViewModel): Observable<MeasurementProcessSettingsViewModel> {
        return this._endPointResolver
            .loadEndPointAsync(null, null)
            .pipe(
                mergeMap(baseUrl => {
                    const fullUrl = `${baseUrl}/api/measurement-process`;
                    return this._httpClient
                        .put<MeasurementProcessSettingsViewModel>(fullUrl, command);
                })
            );
    }

    public loadStatusAsync(): Observable<MeasurementProcessStatuses> {
        return this._endPointResolver
            .loadEndPointAsync(null, null)
            .pipe(
                mergeMap(baseUrl => {
                    const fullUrl = `${baseUrl}/api/measurement-process/status`;
                    return this._httpClient
                        .get<MeasurementProcessStatuses>(fullUrl);
                })
            );
    }

    public startAsync(): Observable<void> {
        return this._endPointResolver
            .loadEndPointAsync(null, null)
            .pipe(
                mergeMap(baseUrl => {
                    const fullUrl = `${baseUrl}/api/measurement-process`;
                    return this._httpClient
                        .post<void>(fullUrl, {});
                })
            );
    }

    public stopAsync(): Observable<void> {
        return this._endPointResolver
            .loadEndPointAsync(null, null)
            .pipe(
                mergeMap(baseUrl => {
                    const fullUrl = `${baseUrl}/api/measurement-process`;
                    return this._httpClient
                        .delete<void>(fullUrl, {});
                })
            );
    }

    //#endregion


}
