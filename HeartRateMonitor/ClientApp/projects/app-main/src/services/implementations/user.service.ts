import {IUserService} from '../interfaces/user-service.interface';
import {Injectable} from '@angular/core';
import {forkJoin, Observable, of} from 'rxjs';
import {AppSettingsService} from './app-settings.service';
import {HttpClient, HttpParams} from '@angular/common/http';
import {LoadUsersViewModel} from '../../view-models/load-users.view-model';
import {delay, flatMap, map, switchMap, tap} from 'rxjs/operators';
import {AppSettings} from '../../models/app-settings.model';
import {UserViewModel} from '../../view-models/user.view-model';
import {SearchResultViewModel} from '../../view-models/search-result.view-model';
import {UserStatuses} from '../../enums/user-status.enum';
import {template, templateSettings} from 'lodash';

@Injectable()
export class UserService implements IUserService {

    //#region Constructor

    public constructor(protected appSettingsService: AppSettingsService,
                       protected httpClient: HttpClient) {
    }

    //#endregion

    //#region Methods

    // Load users by using condition using specific conditions
    public loadUsersAsync(model: LoadUsersViewModel): Observable<SearchResultViewModel<UserViewModel>> {
        return this.appSettingsService
            .getAppSettingsAsync()
            .pipe(
                delay(1000),
                flatMap((appSettings: AppSettings) => {
                    const fullUrl = `${appSettings.baseUrl}/api/users`;
                    // Initialize Params Object
                    let params = new HttpParams();

                    // Begin assigning parameters
                    if (model.keyword && model.keyword.length) {
                        params = params.append('q', model.keyword);
                    }

                    if (!model.page || model.page < 1) {
                        model.page = 1;
                        params = params.append('page', `${model.page}`);
                    } else {
                        params = params.append('page', `${model.page}`);
                    }

                    // Make the API call using the new parameters.
                    return this.httpClient
                        .get(fullUrl, {params})
                        .pipe(
                            map((initialLoadUserResult: any) => {
                                const loadUsersResult = new SearchResultViewModel<UserViewModel>();
                                loadUsersResult.items = initialLoadUserResult.data;
                                loadUsersResult.totalRecords = initialLoadUserResult.total;
                                return loadUsersResult;
                            }),
                            flatMap((loadUserResult: SearchResultViewModel<UserViewModel>) => {
                                let users = loadUserResult.items;
                                const userIdToPhotoMappings = {};
                                let loadUserPhotoObservables: Observable<void>[] = [];

                                for (const user of users) {
                                    const loadUserPhotoObservable = this.loadPhotoUrlAsync(user.avatar)
                                        .pipe(
                                            tap(photoUrl => userIdToPhotoMappings[user.id] = photoUrl),
                                            map(_ => void(0))
                                        );

                                    loadUserPhotoObservables.push(loadUserPhotoObservable);
                                }

                                if (!loadUserPhotoObservables.length) {
                                    loadUserPhotoObservables = [of(void (0))];
                                }

                                return forkJoin(loadUserPhotoObservables)
                                    .pipe(
                                        map(() => {
                                            users = users.map(user => {
                                                return {
                                                    ...user,
                                                    avatar: userIdToPhotoMappings[user.id]
                                                };
                                            });

                                            return new SearchResultViewModel<UserViewModel>(users, loadUserResult.totalRecords);
                                        })
                                    );
                            })
                        );
                })
            );
    }

    // Load users by using id.
    public loadUserAsync(id: string): Observable<UserViewModel> {
        return this.appSettingsService
            .getAppSettingsAsync()
            .pipe(
                flatMap((appSettings: AppSettings) => {

                    // Build request url.
                    const fullUrl = `${appSettings.baseUrl}/api/users/${id}`;

                    // Make the API call using the new parameters.
                    return this.httpClient
                        .get(fullUrl)
                        .pipe(
                            map(loadUserResult => loadUserResult as UserViewModel),
                            map((user: UserViewModel) => {
                                templateSettings.interpolate = /{{([\s\S]+?)}}/g;
                                const compiled = template(appSettings.photoUrl);
                                const photoFullUrl = compiled({image: user.avatar});
                                user.avatar = photoFullUrl;
                                return user;
                            })
                        );
                })
            );
    }

    // Load users by ids asynchronously.
    public loadUsersByIdsAsync(ids: string[]): Observable<UserViewModel[]> {

        // Id is invalid or blank.
        if (!ids || !ids.length) {
            return of(new Array<UserViewModel>());
        }
        return this.appSettingsService
            .getAppSettingsAsync()
            .pipe(
                flatMap((appSettings: AppSettings) => {
                    // Build request url
                    const fullUrl = `${appSettings.baseUrl}/api/users/profile/by-ids`;
                    let params = new HttpParams();

                    for (const userId of ids) {
                        params = params.append('userIds[]', `${userId}`);
                    }

                    return this.httpClient
                        .get<UserViewModel[]>(fullUrl, {params});
                })
            );

    }

    public loadPhotoUrlAsync(photo: string): Observable<string> {
        return this.appSettingsService
            .getAppSettingsAsync()
            .pipe(
                map((appSettings: AppSettings) => {
                    templateSettings.interpolate = /{{([\s\S]+?)}}/g;
                    const compiled = template(appSettings.photoUrl);
                    const photoFullUrl = compiled({image: photo});
                    return photoFullUrl;
                })
            );
    }

    //#endregion


}
