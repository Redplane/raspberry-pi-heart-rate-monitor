import {ISerialPortService} from '../interfaces/serial-port-service.interface';
import {Inject, Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {SelectableItem} from '../../models/selectable-item';
import {AppSettingsService} from './app-settings.service';
import {HttpClient} from '@angular/common/http';
import {mergeMap} from 'rxjs/operators';
import {ENDPOINT_RESOLVER_PROVIDER} from '../../constants/injection-token.constant';
import {IEndPointResolver} from '../interfaces/endpoint.resolver';

@Injectable()
export class SerialPortService implements ISerialPortService {

    //#region Constructor
    public constructor(protected httpClient: HttpClient,
                       @Inject(ENDPOINT_RESOLVER_PROVIDER) protected endPointResolver: IEndPointResolver) {
    }

    //#endregion


    //#region Methods

    // Get available baud rates asynchronously.
    public getAvailableBaudRatesAsync(): Observable<number[]> {
        const availableBaudRates: number[] = [1200, 2400, 4800, 9600, 14400, 19200, 28800, 38400, 56000, 57600, 115200];
        return of(availableBaudRates);
    }

    // Get available parities asynchronously.
    public getAvailableParitiesAsync(): Observable<SelectableItem<number>[]> {
        const availableParities: SelectableItem<number>[] = [];
        availableParities.push(new SelectableItem<number>('TITLE_NONE', 0));
        availableParities.push(new SelectableItem<number>('TITLE_ODD', 1));
        availableParities.push(new SelectableItem<number>('TITLE_EVEN', 2));
        availableParities.push(new SelectableItem<number>('TITLE_MARK', 3));
        availableParities.push(new SelectableItem<number>('TITLE_SPACE', 4));

        return of(availableParities);
    }

    // Get available stop bits asynchronously.
    public getAvailableStopBitsAsync(): Observable<SelectableItem<number>[]> {
        const availableStopBits: SelectableItem<number>[] = [];
        availableStopBits.push(new SelectableItem('TITLE_NONE', 0));
        availableStopBits.push(new SelectableItem('TITLE_ONE', 1));
        availableStopBits.push(new SelectableItem('TITLE_ONE_POINT_FIVE', 3));
        availableStopBits.push(new SelectableItem('TITLE_TWO', 2));

        return of(availableStopBits);
    }

    // Check whether serial port has connected or not.
    public getIsConnectedAsync(name: string): Observable<boolean> {
        return this.endPointResolver
            .loadEndPointAsync(null, null)
            .pipe(
                mergeMap(baseUrl => {
                    const fullUrl = `${baseUrl}/connection/connected?name=${name}`;
                    return this.httpClient.get<boolean>(fullUrl);
                })
            );
    }

    // Get available ports asynchronously.
    public getAvailablePortsAsync(): Observable<string[]> {
        return this.endPointResolver
            .loadEndPointAsync(null, null)
            .pipe(
                mergeMap(baseUrl => {
                    const fullUrl = `${baseUrl}/api/serial-port/available-ports`;
                    return this.httpClient.get<string[]>(fullUrl);
                })
            );
    }

    //#endregion


}
