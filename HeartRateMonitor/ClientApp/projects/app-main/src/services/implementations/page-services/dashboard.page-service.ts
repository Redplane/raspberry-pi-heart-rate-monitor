import {IDashboardPageService} from '../../interfaces/page-services/dashboard-page-service.interface';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {MeasurementDataChart} from '../../../models/measurements/measurement-data-chart';
import {HttpClient} from '@angular/common/http';
import {MeasurementProcessStatuses} from '../../../enums/measurement-process-statuses';

@Injectable()
export class DashboardPageService implements IDashboardPageService {

    //#region Properties

    // tslint:disable-next-line:variable-name
    private _measurementProcessStatus: MeasurementProcessStatuses;

    //#endregion

    //#region Constructor

    public constructor(protected httpClient: HttpClient) {
    }

    //#endregion

    //#region Methods

    public loadMeasurementChartAsync(): Observable<{ [name: string]: MeasurementDataChart }> {
        return this.httpClient
            .get<{ [name: string]: MeasurementDataChart }>(`/assets/measurement-chart-settings.json`);
    }

    public loadMeasurementProcessStatus(): MeasurementProcessStatuses {
        return this._measurementProcessStatus;
    }

    public setMeasurementProcessStatus(value: MeasurementProcessStatuses): void {
        this._measurementProcessStatus = value;
    }

    //#endregion

}
