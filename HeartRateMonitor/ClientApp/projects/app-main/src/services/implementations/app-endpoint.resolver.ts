import {IEndPointResolver} from '../interfaces/endpoint.resolver';
import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {AppSettingsService} from './app-settings.service';

@Injectable()
export class AppEndpointResolver implements IEndPointResolver {

  //#region Constructor

  public constructor(protected appConfigService: AppSettingsService) {
  }

  //#endregion

  //#region Methods

  public loadEndPointAsync(moduleName: string, functionName?: string): Observable<string> {
    const configuration = this.appConfigService.loadStoredConfiguration();
    return of(configuration.baseUrl);
  }

  //#endregion
}
