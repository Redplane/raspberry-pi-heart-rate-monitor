import {IIpcMessageHandler} from '../../../interfaces/ipc-message-handler.interface';
import {Inject, Injectable} from '@angular/core';
import {INgRxMessageBusService, MESSAGE_BUS_SERVICE_PROVIDER} from 'ngrx-message-bus';
import {IncomingIpcMessage} from '../../../models/messages/incoming-ipc-message';
import {MeasurementDoneMessage} from '../../../models/messages/measurement-done-message';
import {IpcMessageTypes} from '../../../constants/ipc-message-event.constant';
import {MeasurementDoneChannelEvent} from '../../../models/channel-events/measurement-done.channel-event';

@Injectable()
export class MeasurementDoneIpcHandler implements IIpcMessageHandler {

    //#region Constructor

    public constructor(@Inject(MESSAGE_BUS_SERVICE_PROVIDER) protected messageBusService: INgRxMessageBusService) {
    }

    //#endregion

    //#region Methods

    public processMessage(message: MessageEvent): void {
        if (!message || !message.data) {
            return;
        }

        const ipcMessage = message.data as IncomingIpcMessage<MeasurementDoneMessage>;
        if (ipcMessage.type !== IpcMessageTypes.measurementDone) {
            return;
        }

        const messageData = ipcMessage.data;
        if (!messageData) {
            return;
        }

        this.messageBusService.addTypedMessage(
            new MeasurementDoneChannelEvent(), messageData);
    }

    //#endregion
}
