import {INavigationService} from '../interfaces/navigation-service.interface';
import {Injectable} from '@angular/core';
import {from, Observable, throwError} from 'rxjs';
import {ScreenCodeConstant} from '../../constants/screen-code.constant';
import {NavigationExtras, Router, UrlTree} from '@angular/router';
import {template, templateSettings} from 'lodash';
import {ExceptionCodeConstant} from '../../constants/exception-code.constant';

@Injectable()
export class NavigationService implements INavigationService {

    //#region Properties

    // Mapping between code & url.
    // tslint:disable-next-line:variable-name
    private readonly _codeToUrl: { [code: string]: string };

    //#endregion

    //#region Constructor

    public constructor(protected router: Router) {

        const codeToUrl = {};
        codeToUrl[ScreenCodeConstant.measurementSnapshots] = '/measurements-snapshots';
        codeToUrl[ScreenCodeConstant.addMeasurementSnapshot] = '/measurements-snapshot';
        codeToUrl[ScreenCodeConstant.editMeasurementSnapshot] = '/measurements-snapshot/{{id}}';
        codeToUrl[ScreenCodeConstant.dashboard] = '/';
        codeToUrl[ScreenCodeConstant.deviceMeasurement] = '/sensor-measurements';
        codeToUrl[ScreenCodeConstant.device] = '/devices';
        codeToUrl[ScreenCodeConstant.addDevice] = '/device';
        codeToUrl[ScreenCodeConstant.editDevice] = '/device/{{id}}';
        codeToUrl[ScreenCodeConstant.stationManagement] = '/stations';
        codeToUrl[ScreenCodeConstant.addStation] = '/station';
        codeToUrl[ScreenCodeConstant.editStation] = '/station/{{id}}';
        this._codeToUrl = codeToUrl;
    }

    //#endregion

    //#region Methods

    // Get the code of screen user is visiting.
    public getCurrentScreenCode(): string {
        return '';
    }

    // Navigate to a specific screen code.
    public navigateToScreenAsync(screenCode: string, routeParams: { [key: string]: any },
                                 extras?: NavigationExtras): Observable<boolean> {

        // Screen code is not found.
        if (!this._codeToUrl || !this._codeToUrl[screenCode]) {
            return throwError(ExceptionCodeConstant.screenCodeNotFound);
        }

        templateSettings.interpolate = /{{([\s\S]+?)}}/g;
        const compiled = template(this._codeToUrl[screenCode]);
        const fullUrl = compiled(routeParams);
        return from(this.router.navigate([fullUrl], extras));
    }

    // Build url tree.
    public buildUrlTree(screenCode: string, routeParams: { [key: string]: any }): UrlTree {

        // Code to url mapping is invalid.
        if (!this._codeToUrl) {
            return null;
        }

        // Screen code is not found.
        if (!this._codeToUrl || !this._codeToUrl[screenCode]) {
            throw new Error(ExceptionCodeConstant.screenCodeNotFound);
        }

        templateSettings.interpolate = /{{([\s\S]+?)}}/g;
        const compiled = template(this._codeToUrl[screenCode]);
        const fullUrl = compiled(routeParams);
        return this.router.createUrlTree([fullUrl]);
    }

    //#endregion


}
