import {IInOutPortService} from '../interfaces/in-out-port-service.interface';
import {Injectable, Injector} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {SerialPortSettings} from '@app/common';
import {IEndPointResolver} from '../interfaces/endpoint.resolver';
import {ENDPOINT_RESOLVER_PROVIDER} from '../../constants/injection-token.constant';
import {catchError, mergeMap} from 'rxjs/operators';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {ExceptionCodeConstant} from '../../constants/exception-code.constant';
import {ItemNotFoundException} from '../../models/exceptions/item-not-found.exception';
import {SetupInOutPortViewModel} from '../../view-models/in-out-ports/setup-in-out-port.view-model';

@Injectable()
export class InOutPortService implements IInOutPortService {

    //#region Properties

    protected endPointResolver: IEndPointResolver;

    protected httpClient: HttpClient;

    //#endregion

    //#region Constructor

    public constructor(injector: Injector) {
        this.endPointResolver = injector.get(ENDPOINT_RESOLVER_PROVIDER);
        this.httpClient = injector.get(HttpClient);
    }

    //#endregion

    //#region Methods

    public getSettingsAsync(): Observable<SerialPortSettings> {
        return this.endPointResolver
            .loadEndPointAsync(null, null)
            .pipe(
                mergeMap(baseUrl => {
                    const fullUrl = `${baseUrl}/api/serial-port/settings`;
                    return this.httpClient
                        .get<SerialPortSettings>(fullUrl);
                }),
                catchError(exception => {
                    if (exception instanceof HttpErrorResponse &&
                        ExceptionCodeConstant.itemNotFound === exception.error.message) {
                        return throwError(new ItemNotFoundException());
                    }

                    return throwError(exception);
                })
            );
    }

    public setupAsync(model: SetupInOutPortViewModel): Observable<SerialPortSettings> {
        return this.endPointResolver
            .loadEndPointAsync(null, null)
            .pipe(
                mergeMap(baseUrl => {
                    const fullUrl = `${baseUrl}/api/serial-port`;
                    return this.httpClient
                        .post<SerialPortSettings>(fullUrl, model);
                })
            );
    }
    //#endregion

}
