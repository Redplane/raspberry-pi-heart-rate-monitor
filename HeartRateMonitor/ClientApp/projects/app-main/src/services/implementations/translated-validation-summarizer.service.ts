import {Inject, Injectable, Optional} from '@angular/core';
import {builtInValidationMessages, VALIDATION_SUMMARIZER_BUILT_IN_MESSAGE_FALLBACK, ValidationMessage} from '@cms-ui/core';
import {TranslateService} from '@ngx-translate/core';
import {ValidationSummarizerService} from '@cms-ui/core';
import {VALIDATION_SUMMARIZER_MESSAGES} from '@cms-ui/core';

@Injectable()
export class TranslatedValidationSummarizerService extends ValidationSummarizerService {

  //#region Constructor

  // tslint:disable-next-line:max-line-length
  public constructor(protected translateService: TranslateService,
                     @Optional() @Inject(VALIDATION_SUMMARIZER_MESSAGES)
                       validatorNameToValidationMessage?: { [name: string]: string; },
                     @Optional() @Inject(VALIDATION_SUMMARIZER_BUILT_IN_MESSAGE_FALLBACK)
                     protected ableToBuiltInMessageFallback?: boolean) {
    super(builtInValidationMessages, validatorNameToValidationMessage, ableToBuiltInMessageFallback);
  }

  //#endregion

  //#region Internal methods

  // Build validation message from specific information.
  protected buildValidationMessage(controlLabel: string, validatorName: string,
                                   additionalValue: { [key: string]: string; }): string {

    if (!this._validatorNameToValidationMessage) {
      return '';
    }


    const initialMessage = this._validatorNameToValidationMessage[validatorName] || this.builtInMessages[validatorName];
    if (!initialMessage) {
      return '';
    }

    const appInitialMessage = `VALIDATION_MESSAGES.${initialMessage}`;
    const translatedControlLabel = this.translateService.instant(controlLabel);
    const translatedMessage = this.translateService.instant(appInitialMessage, {
      additionalValue,
      controlLabel: translatedControlLabel
    });

    return translatedMessage;
  }

  //#endregion
}
