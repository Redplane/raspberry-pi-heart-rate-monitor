import {IMeasurementProcessSettingsDialogService} from '../interfaces/measurement-process-settings-dialog-service.interface';
import {Injectable, Injector} from '@angular/core';
import {from, Observable} from 'rxjs';
import {UpdateMeasurementProcessSettingsViewModel} from '../../view-models/update-measurement-process-settings.view-model';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {MeasurementProcessSettingsDialogComponent} from '../../modules/shared/measurement-process-settings-dialog/measurement-process-settings-dialog.component';
import {MeasurementProcessSettingsViewModel} from '../../view-models/measurement-process-settings.view-model';
import {MEASUREMENT_PROCESS_SETTINGS_PROVIDER} from '../../constants/injection-token.constant';

@Injectable()
export class MeasurementProcessSettingsDialogService implements IMeasurementProcessSettingsDialogService {

    //#region Properties

    protected readonly dialogService: NgbModal;

    //#endregion

    //#region Constructor

    public constructor(protected injector: Injector) {
        this.dialogService = injector.get(NgbModal);
    }

    //#endregion

    //#region Methods

    public displayAsync(initialSettings: MeasurementProcessSettingsViewModel)
        : Observable<UpdateMeasurementProcessSettingsViewModel> {

        const childInjector = Injector.create({
            providers: [
                {
                    provide: MEASUREMENT_PROCESS_SETTINGS_PROVIDER,
                    useValue: initialSettings || new MeasurementProcessSettingsViewModel()
                }
            ],
            parent: this.injector
        });

        const dialogRef = this.dialogService
            .open(MeasurementProcessSettingsDialogComponent, {
                size: 'lg',
                centered: true,
                injector: childInjector
            });

        return from(dialogRef.result);
    }

    //#endregion
}
