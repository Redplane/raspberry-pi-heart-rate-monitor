import {NgModule} from '@angular/core';
import {DashboardComponent} from './dasboard.component';
import {DashboardRoutingModule} from './dashboard-routing.module';
import {CommonModule} from '@angular/common';
import {ChartsModule} from 'ng2-charts';
import {NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule} from '@ngx-translate/core';
import {SerialPortDetailDialogModule} from '../shared/serial-port-detail-dialog/serial-port-detail-dialog.module';
import {MeasurementProcessSettingsDialogModule} from '../shared/measurement-process-settings-dialog/measurement-process-settings-dialog.module';
import {DASHBOARD_PAGE_SERVICE_PROVIDER} from '../../constants/injection-token.constant';
import {DashboardPageService} from '../../services/implementations/page-services/dashboard.page-service';

@NgModule({
    imports: [
        DashboardRoutingModule,
        CommonModule,
        ChartsModule,
        NgbDropdownModule,
        TranslateModule,
        SerialPortDetailDialogModule.forRoot(),
        MeasurementProcessSettingsDialogModule.forRoot()
    ],
    declarations: [
        DashboardComponent
    ],
    exports: [
        DashboardComponent
    ],
    providers: [
        {
            provide: DASHBOARD_PAGE_SERVICE_PROVIDER,
            useClass: DashboardPageService
        }
    ]
})
export class DashboardModule {

}
