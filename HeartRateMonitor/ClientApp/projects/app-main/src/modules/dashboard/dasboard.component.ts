import {ChangeDetectorRef, Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subscription, throwError} from 'rxjs';
import {INgRxMessageBusService, MESSAGE_BUS_SERVICE_PROVIDER} from 'ngrx-message-bus';
import {MeasurementDoneChannelEvent} from '../../models/channel-events/measurement-done.channel-event';
import {MeasurementFactors} from '../../constants/measurement-factors';
import {MeasurementDoneMessage} from '../../models/messages/measurement-done-message';
import {MeasurementDataChart} from '../../models/measurements/measurement-data-chart';
import {IDashboardPageService} from '../../services/interfaces/page-services/dashboard-page-service.interface';
import {
    DASHBOARD_PAGE_SERVICE_PROVIDER,
    IN_OUT_PORT_SERVICE_PROVIDER,
    MEASUREMENT_PROCESS_SETTINGS_DIALOG_SERVICE_PROVIDER,
    MEASUREMENT_PROCESS_SETTINGS_SERVICE_PROVIDER,
    SERIAL_PORT_DETAIL_DIALOG_PROVIDER,
    SERIAL_PORT_SERVICE_PROVIDER
} from '../../constants/injection-token.constant';
import {catchError, map, mergeMap, skipWhile} from 'rxjs/operators';
import * as moment from 'moment';
import {ISerialPortService} from '../../services/interfaces/serial-port-service.interface';
import {ISerialPortDetailDialogService} from '../../services/interfaces/serial-port-detail-dialog-service.interface';
import {IInOutPortService} from '../../services/interfaces/in-out-port-service.interface';
import {ItemNotFoundException} from '../../models/exceptions/item-not-found.exception';
import {AddSerialPortViewModel} from '../../view-models/connection/add-serial-port.view-model';
import {SetupInOutPortViewModel} from '../../view-models/in-out-ports/setup-in-out-port.view-model';
import {EditSerialPortViewModel} from '../../view-models/connection/edit-serial-port.view-model';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {ExceptionCodeConstant} from '../../constants/exception-code.constant';
import {IMeasurementProcessSettingsDialogService} from '../../services/interfaces/measurement-process-settings-dialog-service.interface';
import {IMeasurementProcessSettingsService} from '../../services/interfaces/measurement-process-settings.service';
import {MeasurementProcessStatuses} from '../../enums/measurement-process-statuses';
import {IpcMessageChannelConstant} from '../../constants/ipc-message-channel.constant';
import {IpcRenderer} from 'electron';
import {MeasurementViewModel} from '../../view-models/measurements/measurement.view-model';
import {HubConnection, HubConnectionBuilder} from "@microsoft/signalr";

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'dashboard',
    templateUrl: 'dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

    //#region Properties

    // tslint:disable-next-line:variable-name
    private _nameToMeasurementChart: { [name: string]: MeasurementDataChart };

    // tslint:disable-next-line:variable-name
    private _measurementStatus: MeasurementProcessStatuses;

    // tslint:disable-next-line:variable-name
    private readonly _maxMeasurements = 5;

    private ipc: IpcRenderer;

    private measurementProcess: IpcRenderer;

    //#endregion

    //#region Accessors

    public get measurementNames(): string[] {
        return [MeasurementFactors.dustOne, MeasurementFactors.dustTen,
            MeasurementFactors.dustTwentyFive, MeasurementFactors.battery];
    }

    public get nameToMeasurementChart(): { [name: string]: MeasurementDataChart } {
        return this._nameToMeasurementChart;
    }

    public get measurementStatus(): MeasurementProcessStatuses {
        return this._measurementStatus;
    }

    public get MeasurementStatuses(): typeof MeasurementProcessStatuses {
        return MeasurementProcessStatuses;
    }

    public get chartName(): string {
        return 'ECG';
    }

    //#endregion

    //#region Services

    protected readonly messageBusService: INgRxMessageBusService;
    protected readonly dashboardPageService: IDashboardPageService;
    protected readonly serialPortService: ISerialPortService;
    protected readonly serialPortDetailDialogService: ISerialPortDetailDialogService;
    protected readonly inOutPortService: IInOutPortService;
    protected readonly translatedService: TranslateService;
    protected readonly toastService: ToastrService;
    protected readonly measurementProcessSettingsDialogService: IMeasurementProcessSettingsDialogService;
    protected readonly measurementProcessSettingsService: IMeasurementProcessSettingsService;
    protected _hubConnection: HubConnection;

    // Subscription watch list.
    protected readonly subscription: Subscription;

    //#endregion

    //#region Constructor

    public constructor(private injector: Injector) {

        // Initialize measurement chart.
        this._nameToMeasurementChart = {};

        this._measurementStatus = MeasurementProcessStatuses.unavailable;

        // Service resolve.
        this.messageBusService = injector.get(MESSAGE_BUS_SERVICE_PROVIDER);
        this.dashboardPageService = injector.get(DASHBOARD_PAGE_SERVICE_PROVIDER);
        this.serialPortService = injector.get(SERIAL_PORT_SERVICE_PROVIDER);
        this.serialPortDetailDialogService = injector.get(SERIAL_PORT_DETAIL_DIALOG_PROVIDER);
        this.inOutPortService = injector.get(IN_OUT_PORT_SERVICE_PROVIDER);
        this.translatedService = injector.get(TranslateService);
        this.toastService = injector.get(ToastrService);
        this.measurementProcessSettingsDialogService = injector.get(MEASUREMENT_PROCESS_SETTINGS_DIALOG_SERVICE_PROVIDER);
        this.measurementProcessSettingsService = injector.get(MEASUREMENT_PROCESS_SETTINGS_SERVICE_PROVIDER);

        if ((window as any).require) {
            try {
                this.ipc = (window as any).require('electron').ipcRenderer;
            } catch (error) {
                throw error;
            }
        } else {
            console.warn('Could not load electron ipc');
        }

        // Subscription watch list.
        this.subscription = new Subscription();
    }

    //#endregion

    //#region Methods

    // Called when component is initialized.
    public ngOnInit(): void {

        this._measurementStatus = this.dashboardPageService.loadMeasurementProcessStatus();

        const loadMeasurementChartSubscription = this.dashboardPageService
            .loadMeasurementChartAsync()
            .subscribe(value => {
                this._nameToMeasurementChart = value;

                this._hubConnection = new HubConnectionBuilder()
                    .withAutomaticReconnect()
                    .withUrl('/signalr-hub/measurement')
                    .build();

                this._hubConnection
                    .on('OnMeasurementDone', measurement => {
                        const measurementDoneChannelEvent = new MeasurementDoneChannelEvent();
                        measurement['name'] = 'ECG';
                        const measurementDoneMessage = new MeasurementDoneMessage([measurement], measurement.time);
                        this.messageBusService.addTypedMessage(measurementDoneChannelEvent, measurementDoneMessage);
                    });

                // Start the connection.
                this._hubConnection.start();
            });
        this.subscription.add(loadMeasurementChartSubscription);

        const hookMeasurementDoneMessageSubscription = this.messageBusService
            .hookTypedMessageChannel(new MeasurementDoneChannelEvent())
            .pipe(
                skipWhile(_ => !this._nameToMeasurementChart)
            )
            .subscribe((message: MeasurementDoneMessage) => {

                // Get measurements.
                if (!message || !message.measurements || !message.measurements.length) {
                    return;
                }

                const time = moment(message.time).format('DD/MM/YYYY HH:mm:ss');
                for (const measurement of message.measurements) {

                    const measurementChart = this._nameToMeasurementChart[measurement.name];
                    if (!measurementChart) {
                        continue;
                    }

                    // this._nameToMeasurementChart[measurement.name] = measurement;
                    const dataSets = measurementChart.dataSets;
                    if (dataSets.length !== 1) {
                        continue;
                    }

                    const histories = dataSets[0];
                    if (histories.data.length > this._maxMeasurements) {
                        histories.data.shift();
                        measurementChart.labels.shift();
                    }

                    histories.data.push(measurement.value);
                    measurementChart.labels.push(time);

                    const changeDetectorRef = this.injector.get(ChangeDetectorRef);
                    changeDetectorRef.detectChanges();
                }
            });

        this.subscription.add(hookMeasurementDoneMessageSubscription);


    }

    // Called when component is destroyed.
    public ngOnDestroy(): void {

        if (this.subscription && !this.subscription.closed) {
            this.subscription.unsubscribe();
        }

        if (this.measurementProcess) {
            this.measurementProcess.removeAllListeners(IpcMessageChannelConstant.ecg);
        }

        if (this._hubConnection) {
            this._hubConnection.stop();
            this._hubConnection = null;
        }
    }

    // Called when output serial port setup is called.
    public doSerialPortSetup(): void {

        const doOutputSerialSetupSubscription = this.inOutPortService
            .getSettingsAsync()
            .pipe(
                mergeMap(settings => {
                    return this.serialPortDetailDialogService
                        .displayEditSerialPortDialogAsync(settings);
                }),
                catchError(exception => {
                    if (exception instanceof ItemNotFoundException) {
                        return this.serialPortDetailDialogService
                            .displayAddSerialPortDialogAsync();
                    }

                    return throwError(exception);
                }),
                mergeMap(model => {

                    let setupCommand: SetupInOutPortViewModel = null;

                    if (model instanceof AddSerialPortViewModel) {
                        const addSerialPortCommand = model as AddSerialPortViewModel;
                        setupCommand = new SetupInOutPortViewModel(addSerialPortCommand);
                        return this.inOutPortService
                            .setupAsync(setupCommand);
                    }

                    const editSerialPortCommand = model as EditSerialPortViewModel;
                    setupCommand = new SetupInOutPortViewModel(editSerialPortCommand);
                    return this.inOutPortService
                        .setupAsync(setupCommand);
                }),
            )
            .subscribe(
                () => {

                    const translatedMessage = this.translatedService
                        .instant('DASHBOARD_PAGE.INPUT_SERIAL_PORT_SETUP_SUCCESSFULLY');
                    this.toastService.success(translatedMessage);
                },
                error => {

                    if (ExceptionCodeConstant.dialogDismiss === error) {
                        return;
                    }

                    const translatedMessage = this.translatedService
                        .instant('DASHBOARD_PAGE.INPUT_SERIAL_PORT_SETUP_UNSUCCESSFULLY');

                    this.toastService.error(translatedMessage);
                });
        this.subscription.add(doOutputSerialSetupSubscription);
    }

    public clickSetupMeasurementProcess(): void {

        const setupMeasurementProcessSubscription =
            this.measurementProcessSettingsService.loadSettingsAsync()
                .pipe(
                    mergeMap(settings => {
                        return this.measurementProcessSettingsDialogService
                            .displayAsync(settings);
                    }),
                    mergeMap(command => {
                        return this.measurementProcessSettingsService.editSettingsAsync(command);
                    })
                )
                .subscribe();

        this.subscription.add(setupMeasurementProcessSubscription);
    }

    public clickStartStopMeasurement(): void {

        let startStopMeasurementSubscription: Observable<MeasurementProcessStatuses> = null;

        if (this.measurementStatus !== this.MeasurementStatuses.running) {
            startStopMeasurementSubscription = this.measurementProcessSettingsService
                .startAsync()
                .pipe(
                    map(() => MeasurementProcessStatuses.running)
                );
        } else {
            startStopMeasurementSubscription = this.measurementProcessSettingsService
                .stopAsync()
                .pipe(
                    map(() => MeasurementProcessStatuses.stopped)
                );
        }

        if (startStopMeasurementSubscription !== null) {
            startStopMeasurementSubscription
                .subscribe(designatedMeasurementStatus => {
                    this._measurementStatus = designatedMeasurementStatus;

                    let message = '';
                    if (this._measurementStatus === MeasurementProcessStatuses.stopped) {
                        message = 'MSG_MEASUREMENT_STOPPED';
                    } else {
                        message = 'MSG_MEASUREMENT_STARTED';
                    }

                    const translatedMessage = this.translatedService.instant(message);
                    this.toastService.success(translatedMessage);
                });
        }
    }

    //#endregion

}
