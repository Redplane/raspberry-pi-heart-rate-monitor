import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './dasboard.component';
import {DashboardPageResolve} from '../../resolves/dashboard.page-resolve';

const routes: Routes = [
    {
        path: '',
        component: DashboardComponent,
        resolve: {
            initialResolve: DashboardPageResolve
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [
        RouterModule
    ],
    providers: [
        DashboardPageResolve
    ]
})
export class DashboardRoutingModule {

}
