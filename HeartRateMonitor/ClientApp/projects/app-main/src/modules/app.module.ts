import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {AppComponent} from './app.component';
import {ServicesModule} from '../services/services.module';
import {InterceptorsModule} from '../interceptors/interceptors.module';
import {DirectivesModule} from '../directives/directives.module';
import {AppRoutingModule} from './app-routing.module';
import {AppSettingsService} from '../services/implementations/app-settings.service';
import {loadAppSettingsAsync} from '../factories/load-app-settings.factory';
import {CommonModule} from '@angular/common';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpLoaderFactory} from '../factories/http-loader.factory';
import {StorageModule} from '@ngx-pwa/local-storage';
import {ReactiveFormsModule} from '@angular/forms';
import {ToastrModule} from 'ngx-toastr';
import {DashboardGuard} from '../guards/dashboard.guard';
import {DeviceMeasurementGuard} from '../guards/device-measurement.guard';
import {SpinnerContainerModule} from '@cms-ui/core';

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        CommonModule,
        BrowserAnimationsModule,
        BrowserModule,
        HttpClientModule,
        ServicesModule,
        InterceptorsModule,
        DirectivesModule,
        AppRoutingModule,
        ReactiveFormsModule,
        SpinnerContainerModule.forRoot(),
        ToastrModule.forRoot({
            preventDuplicates: true
        }),
        StorageModule.forRoot({
            IDBNoWrap: true
        }),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            },
            defaultLanguage: 'en-US'
        })
    ],
    bootstrap: [AppComponent],
    providers: [
        AppSettingsService,
        {
            provide: APP_INITIALIZER,
            useFactory: loadAppSettingsAsync,
            multi: true,
            deps: [AppSettingsService]
        },
        DashboardGuard,
        DeviceMeasurementGuard
    ]
})
export class AppModule {
}
