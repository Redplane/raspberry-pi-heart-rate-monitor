import {Component, InjectFlags, Injector} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {IValidationSummarizerService, VALIDATION_SUMMARIZER_PROVIDER} from '@cms-ui/core';
import {UpdateMeasurementProcessSettingsViewModel} from '../../../view-models/update-measurement-process-settings.view-model';
import {EditableField} from '@app/common';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {MeasurementProcessSettingsViewModel} from '../../../view-models/measurement-process-settings.view-model';
import {cloneDeep} from 'lodash-es';
import {MEASUREMENT_PROCESS_SETTINGS_PROVIDER} from '../../../constants/injection-token.constant';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'measurement-process-settings-dialog',
    templateUrl: 'measurement-process-settings-dialog.component.html',
    styleUrls: ['./measurement-process-settings-dialog.component.css']
})
export class MeasurementProcessSettingsDialogComponent {

    //#region Properties

    // tslint:disable-next-line:variable-name
    protected readonly _initialSettings: MeasurementProcessSettingsViewModel;

    public readonly measurementSettingsForm: FormGroup;

    public readonly timeoutControl: FormControl;

    public readonly intervalControl: FormControl;

    public readonly slaveIdControl: FormControl;

    //#endregion

    //#region Services

    protected readonly validationSummarizerService: IValidationSummarizerService;

    protected readonly activeModal: NgbActiveModal;

    //#endregion

    //#region Constructor

    public constructor(injector: Injector) {

        const initialSettings = injector.get(MEASUREMENT_PROCESS_SETTINGS_PROVIDER, null, InjectFlags.Optional)
            || new MeasurementProcessSettingsViewModel();
        this._initialSettings = cloneDeep(initialSettings);

        this.timeoutControl = new FormControl(initialSettings.timeout);
        this.intervalControl = new FormControl(initialSettings.interval);
        this.slaveIdControl = new FormControl(initialSettings.slaveId);

        this.measurementSettingsForm = new FormGroup({
            timeout: this.timeoutControl,
            interval: this.intervalControl,
            slaveId: this.slaveIdControl
        });

        this.validationSummarizerService = injector.get(VALIDATION_SUMMARIZER_PROVIDER);
        this.activeModal = injector.get(NgbActiveModal);
    }

    //#endregion

    //#region Methods

    public clickOk(): void {

        // Invalid form.
        if (!this.measurementSettingsForm) {
            return;
        }

        this.validationSummarizerService.doFormControlsValidation(this.measurementSettingsForm);
        if (!this.measurementSettingsForm.valid) {
            return;
        }

        const command = new UpdateMeasurementProcessSettingsViewModel();
        command.slaveId = new EditableField<number>(this.slaveIdControl.value, this.slaveIdControl.dirty);
        command.interval = new EditableField<number>(this.intervalControl.value, this.intervalControl.dirty);
        command.timeout = new EditableField<number>(this.timeoutControl.value, this.timeoutControl.dirty);

        this.activeModal.close(command);
    }

    public clickCancel(): void {
        this.activeModal.dismiss();
    }

    //#endregion
}
