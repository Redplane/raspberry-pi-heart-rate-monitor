import {ModuleWithProviders, NgModule} from '@angular/core';
import {MeasurementProcessSettingsDialogComponent} from './measurement-process-settings-dialog.component';
import {ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {ValidationSummarizerModule} from '@cms-ui/core';
import {MEASUREMENT_PROCESS_SETTINGS_DIALOG_SERVICE_PROVIDER} from '../../../constants/injection-token.constant';
import {MeasurementProcessSettingsDialogService} from '../../../services/implementations/measurement-process-settings-dialog.service';
import {CommonModule} from '@angular/common';

@NgModule({
    declarations: [
        MeasurementProcessSettingsDialogComponent
    ],
    exports: [
        MeasurementProcessSettingsDialogComponent
    ],
    imports: [
        ReactiveFormsModule,
        TranslateModule,
        ValidationSummarizerModule,
        CommonModule
    ],
    entryComponents: [
        MeasurementProcessSettingsDialogComponent
    ]
})
export class MeasurementProcessSettingsDialogModule {

    public static forRoot(): ModuleWithProviders<MeasurementProcessSettingsDialogModule> {
        return {
            ngModule: MeasurementProcessSettingsDialogModule,
            providers: [
                {
                    provide: MEASUREMENT_PROCESS_SETTINGS_DIALOG_SERVICE_PROVIDER,
                    useClass: MeasurementProcessSettingsDialogService
                }
            ]
        };
    }
}
