import {Component, Inject, OnInit, Optional} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, NgControl, Validators} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {SelectableItem} from '../../../models/selectable-item';
import {
    INITIAL_SERIAL_PORT_PROVIDER, SERIAL_PORT_AVAILABLE_PORT_PROVIDER,
    SERIAL_PORT_SERVICE_PROVIDER
} from '../../../constants/injection-token.constant';
import {ISerialPortService} from '../../../services/interfaces/serial-port-service.interface';
import {Observable, Subscription} from 'rxjs';
import {INgRxMessageBusService, MESSAGE_BUS_SERVICE_PROVIDER} from 'ngrx-message-bus';
import {debounceTime, distinctUntilChanged, map, switchMap, tap} from 'rxjs/operators';
import {IValidationSummarizerService, VALIDATION_SUMMARIZER_PROVIDER} from '@cms-ui/core';
import {EditableField, SerialPortSettings} from '@app/common';
import {AddSerialPortViewModel} from '../../../view-models/connection/add-serial-port.view-model';
import {EditSerialPortViewModel} from '../../../view-models/connection/edit-serial-port.view-model';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'serial-port-detail-dialog',
    templateUrl: 'serial-port-detail-dialog.component.html',
    styleUrls: ['serial-port-detail-dialog.component.scss']
})
export class SerialPortDetailDialogComponent implements OnInit {

    //#region Properties

    /* Control of name field*/
    // tslint:disable-next-line:variable-name
    private readonly _nameControl: FormControl;

    /* Control of baud rate field*/
    // tslint:disable-next-line:variable-name
    private readonly _baudRateControl: FormControl;

    /* Control of data bit field*/
    // tslint:disable-next-line:variable-name
    private readonly _dataBitControl: FormControl;

    /* Control of parity field*/
    // tslint:disable-next-line:variable-name
    private readonly _parityControl: FormControl;

    /* Control of stop bit field*/
    // tslint:disable-next-line:variable-name
    private readonly _stopBitControl: FormControl;

    // Auto connect control.
    // tslint:disable-next-line:variable-name
    private readonly _autoConnectControl: FormControl;

    /* Form group declaration*/
    public readonly serialPortDetailFormGroup: FormGroup;

    // List of available baud rates.
    // tslint:disable-next-line:variable-name
    private _availableBaudRates: number[];

    // List of available parities.
    // tslint:disable-next-line:variable-name
    private _availableParities: SelectableItem<number>[];

    // List of available stop bits.
    // tslint:disable-next-line:variable-name
    private _availableStopBits: SelectableItem<number>[];

    // Available ports.
    // tslint:disable-next-line:variable-name
    private _availablePorts: string[];

    // Options for auto connect.
    // tslint:disable-next-line:variable-name
    private readonly _autoConnectOptions: SelectableItem<boolean>[];

    // tslint:disable-next-line:variable-name
    private _subscription: Subscription;

    //#endregion

    //#region Accessors

    public get availablePorts(): string[] {
        return this._availablePorts;

    }

    /* Name control*/
    public get nameControl(): FormControl {
        return this._nameControl;
    }

    /* Baud rate control*/
    public get baudRateControl(): FormControl {
        return this._baudRateControl;
    }

    /* Data bit control*/
    public get dataBitControl(): FormControl {
        return this._dataBitControl;
    }

    /* Parity control*/
    public get parityControl(): FormControl {
        return this._parityControl;
    }

    /* Stop bit control*/
    public get stopBitControl(): FormControl {
        return this._stopBitControl;
    }

    // Auto connect control.
    public get autoConnectControl(): FormControl {
        return this._autoConnectControl;
    }

    // Get available baud rates for selection.
    public get availableBaudRates(): number[] {
        return this._availableBaudRates;
    }

    // Get available parities for selection.
    public get availableParities(): SelectableItem<number>[] {
        return this._availableParities;
    }

    // Get available stop bits for selection.
    public get availableStopBits(): SelectableItem<number>[] {
        return this._availableStopBits;
    }

    // Options for auto connect.
    public get autoConnectOptions(): SelectableItem<boolean>[] {
        return this._autoConnectOptions;
    }

    public get availableDataBits(): number[] {
        return [7, 8];
    }

    public readonly getAvailableSerialPortHandler = (keywordPopulatedEvent: Observable<string>) =>
        keywordPopulatedEvent.pipe(
            debounceTime(250),
            distinctUntilChanged(),
            switchMap(keyword => this.serialPortService.getAvailablePortsAsync()
                .pipe(
                    map(availablePorts => !keyword ? availablePorts.slice(0, 10)
                        : availablePorts.filter(v => v.toLowerCase().indexOf(keyword.toLowerCase()) > -1).slice(0, 10))
                ))
        )

    //#endregion

    //#region Constructor

    public constructor(@Optional() @Inject(INITIAL_SERIAL_PORT_PROVIDER) public readonly initialSerialPort: SerialPortSettings,
                       @Inject(SERIAL_PORT_SERVICE_PROVIDER) protected serialPortService: ISerialPortService,
                       @Inject(MESSAGE_BUS_SERVICE_PROVIDER) protected messageBusService: INgRxMessageBusService,
                       @Inject(VALIDATION_SUMMARIZER_PROVIDER) protected validationSummarizerService: IValidationSummarizerService,
                       @Inject(SERIAL_PORT_AVAILABLE_PORT_PROVIDER) protected availableSerialPorts: string[],
                       protected activeModal: NgbActiveModal) {

        this._availablePorts = availableSerialPorts;

        this._autoConnectOptions = new Array<SelectableItem<boolean>>();
        this._autoConnectOptions.push(new SelectableItem<boolean>('TITLE_YES', true));
        this._autoConnectOptions.push(new SelectableItem<boolean>('TITLE_NO', false));

        this._availableBaudRates = [];

        // Name control.
        this._nameControl = new FormControl('', {
            validators: [
                Validators.required
            ]
        });

        this._baudRateControl = new FormControl('');
        this._dataBitControl = new FormControl('');
        this._parityControl = new FormControl('');
        this._stopBitControl = new FormControl('');
        this._autoConnectControl = new FormControl('');

        this.serialPortDetailFormGroup = new FormGroup({
            name: this._nameControl,
            baudRate: this.baudRateControl,
            dataBit: this.dataBitControl,
            parity: this._parityControl,
            stopBit: this.stopBitControl
        });

        this._subscription = new Subscription();

    }

    //#endregion

    //#region Methods

    public ngOnInit(): void {

        const hookGetAvailableBaudRateSubscription = this.serialPortService
            .getAvailableBaudRatesAsync()
            .subscribe(availableBaudRates => {
                this._availableBaudRates = availableBaudRates;
            });

        const hookGetAvailableParitiesSubscription = this.serialPortService.getAvailableParitiesAsync()
            .subscribe((availableParities: SelectableItem<number>[]) => {
                this._availableParities = availableParities;
            });

        const hookGetAvailableStopBitsSubscription = this.serialPortService.getAvailableStopBitsAsync()
            .subscribe((availableStopBits: SelectableItem<number>[]) => {
                this._availableStopBits = availableStopBits;
            });

        if (!this.initialSerialPort) {
            // Setup controls' initial value.
            this.nameControl.patchValue(null);
            this.baudRateControl.patchValue(null);
            this.dataBitControl.patchValue(null);
            this.parityControl.patchValue(null);
            this.stopBitControl.patchValue(null);
            this.autoConnectControl.patchValue(false);
        } else {
            const serialPortSettings = this.initialSerialPort;
            this.nameControl.patchValue(serialPortSettings.name);
            this.baudRateControl.patchValue(serialPortSettings.baudRate);
            this.dataBitControl.patchValue(serialPortSettings.dataBit);
            this.parityControl.patchValue(serialPortSettings.parity);
            this.stopBitControl.patchValue(serialPortSettings.stopBits);
            this.autoConnectControl.patchValue(serialPortSettings.autoStart || false);
        }

        this._subscription.add(hookGetAvailableBaudRateSubscription);
        this._subscription.add(hookGetAvailableParitiesSubscription);
        this._subscription.add(hookGetAvailableStopBitsSubscription);
    }

    // Called when OK button is clicked.
    public clickOk(): void {

        // Trigger form validation.
        this.validationSummarizerService.doFormControlsValidation(this.serialPortDetailFormGroup);

        // Form is invalid.
        if (this.serialPortDetailFormGroup.invalid) {
            return;
        }

        // In add mode.
        if (!this.initialSerialPort) {
            const addSerialPortCommand = new AddSerialPortViewModel();

            addSerialPortCommand.name = this.nameControl.value;
            addSerialPortCommand.baudRate = this.baudRateControl.value;
            addSerialPortCommand.dataBit = this.dataBitControl.value;
            addSerialPortCommand.parity = this.parityControl.value;
            addSerialPortCommand.stopBits = this.stopBitControl.value;
            addSerialPortCommand.autoStart = this.autoConnectControl.value;

            // Add new serial port
            this.activeModal.close(addSerialPortCommand);
            return;
        }

        const editSerialPortCommand = new EditSerialPortViewModel(this.initialSerialPort.name);

        editSerialPortCommand.name = new EditableField<string>(this.nameControl.value, this.nameControl.dirty);
        editSerialPortCommand.baudRate = new EditableField<number>(this.baudRateControl.value, this.baudRateControl.dirty);
        editSerialPortCommand.dataBit = new EditableField<number>(this.dataBitControl.value, this.dataBitControl.dirty);
        editSerialPortCommand.parity = new EditableField<number>(this.parityControl.value, this.parityControl.dirty);
        editSerialPortCommand.stopBit = new EditableField<number>(this.stopBitControl.value, this.stopBitControl.dirty);
        editSerialPortCommand.autoStart = new EditableField<boolean>(this.autoConnectControl.value, this.autoConnectControl.dirty);

        // Update existed serial port
        this.activeModal.close(editSerialPortCommand);
    }

    // Called when cancel button is clicked.
    public clickCancel(): void {
        this.activeModal.dismiss();
    }

    //#endregion

}
