import {ModuleWithProviders, NgModule} from '@angular/core';
import {SerialPortDetailDialogComponent} from './serial-port-detail-dialog.component';
import {CommonModule} from '@angular/common';
import {NgbModalModule, NgbTypeaheadModule} from '@ng-bootstrap/ng-bootstrap';
import {SerialPortDetailDialogService} from '../../../services/implementations/serial-port-detail-dialog.service';
import {SERIAL_PORT_DETAIL_DIALOG_PROVIDER} from '../../../constants/injection-token.constant';
import {TranslateModule} from '@ngx-translate/core';
import {ReactiveFormsModule} from '@angular/forms';
import {SpinnerContainerModule, VALIDATION_SUMMARIZER_PROVIDER, ValidationSummarizerModule} from '@cms-ui/core';
import {TranslatedValidationSummarizerService} from '../../../services/implementations/translated-validation-summarizer.service';
import {NgSelectModule} from '@ng-select/ng-select';

@NgModule({
    imports: [
        CommonModule,
        NgbModalModule,
        TranslateModule,
        ReactiveFormsModule,
        NgbTypeaheadModule,
        ValidationSummarizerModule,
        SpinnerContainerModule,
        NgSelectModule
    ],
    declarations: [
        SerialPortDetailDialogComponent
    ],
    exports: [
        SerialPortDetailDialogComponent
    ]
})
export class SerialPortDetailDialogModule {

    public static forRoot(): ModuleWithProviders<SerialPortDetailDialogModule> {
        return {
            ngModule: SerialPortDetailDialogModule,
            providers: [
                {
                    provide: SERIAL_PORT_DETAIL_DIALOG_PROVIDER,
                    useClass: SerialPortDetailDialogService
                },
                {
                    provide: VALIDATION_SUMMARIZER_PROVIDER,
                    useClass: TranslatedValidationSummarizerService
                }
            ]
        };
    }
}
