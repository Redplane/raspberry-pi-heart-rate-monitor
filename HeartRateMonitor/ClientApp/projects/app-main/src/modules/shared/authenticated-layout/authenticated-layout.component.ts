import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {IAuthenticationService} from '../../../services/interfaces/authentication-service.interface';
import {switchMap} from 'rxjs/operators';
import {
    AUTHENTICATION_SERVICE_INJECTOR,
    NAVIGATION_SERVICE_PROVIDER
} from '../../../constants/injection-token.constant';
import {INgRxMessageBusService, MESSAGE_BUS_SERVICE_PROVIDER} from 'ngrx-message-bus';
import {of, Subscription} from 'rxjs';
import {ToBase64Pipe} from '../../../pipes/to-base64.pipe';
import {IUser} from '../../../interfaces/user.interface';
import {MessageChannelNameConstant} from '../../../constants/message-channel-name.constant';
import {MessageEventNameConstant} from '../../../constants/message-event-name.constant';
import {ProfileViewModel} from '../../../view-models/user/profile.view-model';
import {BreadcrumbItem} from '../../../models/breadcrumb-item';
import {INavigationService} from '../../../services/interfaces/navigation-service.interface';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'authenticated-layout',
    templateUrl: './authenticated-layout.component.html',
    styleUrls: ['./authenticated-layout.component.scss']
})
export class AuthenticatedLayoutComponent implements OnInit, OnDestroy {

    //#region Properties

    /*
    * User profile photo.
    * */
    // tslint:disable-next-line:variable-name
    private _photo: string;

    /*
    * Whether profile menu should be displayed or not.
    * */
    public shouldProfileMenuDisplayed: boolean;

    /*
    * Time when profile menu is displayed.
    * */
    // tslint:disable-next-line:variable-name
    private _profileMenuDisplayedTime: number | null = null;

    /*
    * Subscription management.
    * */
    // tslint:disable-next-line:variable-name
    private readonly _subscription: Subscription;

    /*
    * User profile.
    * */
    // tslint:disable-next-line:variable-name
    private _profile: IUser;

    // tslint:disable-next-line:variable-name
    private _breadcrumbItems: BreadcrumbItem[];

    // Page heading.
    // tslint:disable-next-line:variable-name
    private _heading: string;
    //#endregion

    //#region Accessors

    /*
    * User profile photo.
    * */
    public get photo(): string {
        return this._photo;
    }

    /*
    * Profile information.
    * */
    public get profile(): IUser {
        return this._profile;
    }

    public get breadCrumbItems(): BreadcrumbItem[] {
        if (!this._breadcrumbItems) {
            this._breadcrumbItems = [];
        }

        return this._breadcrumbItems;
    }

    // Get page heading.
    public get heading(): string {
        return this._heading;
    }

    //#endregion

    //#region Constructor

    // tslint:disable-next-line:max-line-length
    public constructor(@Inject(AUTHENTICATION_SERVICE_INJECTOR) protected authenticationService: IAuthenticationService,
                       @Inject(MESSAGE_BUS_SERVICE_PROVIDER) protected messageBusService: INgRxMessageBusService,
                       @Inject(NAVIGATION_SERVICE_PROVIDER) protected navigationService: INavigationService,
                       protected router: Router,
                       protected toBase64Pipe: ToBase64Pipe,
                       protected changeDetectorRef: ChangeDetectorRef) {

        // Default user profile photo.
        this._photo = '/assets/images/user-default.svg';
        this._subscription = new Subscription();
    }

    //#endregion

    //#region Methods

    // Called when component is initialized.
    public ngOnInit() {

        const loadUserProfileSubscription = this
            .messageBusService
            .hookMessageChannel<ProfileViewModel>(MessageChannelNameConstant.ui, MessageEventNameConstant.updateUserProfile)
            .pipe(
                switchMap(user => {
                    this._profile = user as IUser;
                    this._photo = user.avatar;
                    this.changeDetectorRef.detectChanges();
                    return of(true);
                })
            )
            .subscribe();

        const hookBreadcrumbItemsUpdateSubscription = this.messageBusService
            .hookMessageChannel<BreadcrumbItem[]>(MessageChannelNameConstant.ui, MessageEventNameConstant.updateBreadcrumb)
            .subscribe(breadcrumbs => {
                this._breadcrumbItems = breadcrumbs;
                this.changeDetectorRef.detectChanges();
            });

        const hookPageHeadingUpdateSubscription = this.messageBusService
            .hookMessageChannel<string>(MessageChannelNameConstant.ui, MessageEventNameConstant.updatePageHeading)
            .subscribe(heading => {
                this._heading = heading;
                this.changeDetectorRef.detectChanges();
            });


        this._subscription.add(loadUserProfileSubscription);
        this._subscription.add(hookBreadcrumbItemsUpdateSubscription);
        this._subscription.add(hookPageHeadingUpdateSubscription);
        // this._subscription.add(loadUserProfilePhotoSubscription);
    }

    /*
    * Called when component is destroyed.
    * */
    public ngOnDestroy(): void {
        if (this._subscription && !this._subscription.closed) {
            this._subscription.unsubscribe();
        }
    }

    //#endregion
}
