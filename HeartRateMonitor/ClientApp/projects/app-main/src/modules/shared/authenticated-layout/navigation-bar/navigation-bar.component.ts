import {Component, Inject, Input, OnDestroy, OnInit} from '@angular/core';
import {filter, flatMap, map, switchMap, takeUntil} from 'rxjs/operators';
import {from, Subject, Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {LocalStorageKeyConstant} from '../../../../constants/local-storage-key.constant';
import {UserViewModel} from '../../../../view-models/user.view-model';
import {
    AUTHENTICATION_SERVICE_INJECTOR,
    NAVIGATION_SERVICE_PROVIDER
} from '../../../../constants/injection-token.constant';
import {INavigationService} from '../../../../services/interfaces/navigation-service.interface';
import {ScreenCodeConstant} from '../../../../constants/screen-code.constant';
import {IAuthenticationService} from '../../../../services/interfaces/authentication-service.interface';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'navigation-bar',
    templateUrl: './navigation-bar.component.html',
    styleUrls: ['./navigation-bar.component.scss']
})
export class NavigationBarComponent implements OnInit, OnDestroy {

    //#region Properties

    // Subscription watch list.
    // tslint:disable-next-line:variable-name
    private readonly _subscription: Subscription;

    // User profile.
    // tslint:disable-next-line:variable-name
    private _user: UserViewModel;

    //#endregion

    //#region Accessors

    // Tag which is defined for user menu.
    public get userMenuTag(): string {
        return 'USER_MENU_TAG';
    }

    // Get user profile.
    public get user(): UserViewModel {

        if (!this._user) {
            this._user = new UserViewModel();
        }

        return this._user;
    }

    // Set user profile.
    @Input('user')
    public set user(value: UserViewModel) {
        this._user = value;
    }

    //#endregion

    private destroy$: Subject<void> = new Subject<void>();
    userPictureOnly = false;

    currentTheme = 'default';

    userMenu = [{title: 'Profile', data: 'ACTION_VIEW_PROFILE'}, {title: 'Log out', data: 'ACTION_LOGOUT'}];

    constructor( @Inject(NAVIGATION_SERVICE_PROVIDER) protected navigationService: INavigationService,
                @Inject(AUTHENTICATION_SERVICE_INJECTOR) protected authenticationService: IAuthenticationService,
                protected router: Router) {

        this._user = new UserViewModel();
        this._subscription = new Subscription();
    }

    // Called when component is initialized.
    public ngOnInit() {
    }

    // Called when component is destroyed.
    public ngOnDestroy() {

        if (this._subscription && !this._subscription.closed) {
            this._subscription.unsubscribe();
        }

        this.destroy$.next();
        this.destroy$.complete();
    }
}
