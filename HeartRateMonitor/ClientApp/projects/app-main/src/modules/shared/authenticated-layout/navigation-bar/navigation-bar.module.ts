import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NavigationBarComponent} from './navigation-bar.component';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {FormsModule} from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        TranslateModule,
        FormsModule,
        TranslateModule.forChild()
    ],
    declarations: [
        NavigationBarComponent
    ],
    exports: [
        NavigationBarComponent
    ]
})
export class NavigationBarModule {

}
