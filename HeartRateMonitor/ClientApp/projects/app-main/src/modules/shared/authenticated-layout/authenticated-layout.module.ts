import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthenticatedLayoutComponent} from './authenticated-layout.component';
import {RouterModule} from '@angular/router';
import {DirectivesModule} from '../../../directives/directives.module';
import {PipesModule} from '../../../pipes/pipes.module';
import {NavigationBarModule} from './navigation-bar/navigation-bar.module';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,

        DirectivesModule,
        PipesModule,
        NavigationBarModule,
        TranslateModule.forChild()
    ],
    providers: [
    ],
    declarations: [
        AuthenticatedLayoutComponent
    ]
})
export class AuthenticatedLayoutModule {
}
