import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NgRxMessageBusModule} from 'ngrx-message-bus';
import {TranslateModule} from '@ngx-translate/core';
import {WINDOW_PROVIDERS} from '../services/implementations/window.service';
import {MomentModule} from 'ngx-moment';
import {ResolveModule} from '../resolves/resolve.module';
import {DashboardGuard} from '../guards/dashboard.guard';
import {AuthenticatedLayoutComponent} from './shared/authenticated-layout/authenticated-layout.component';
import {AuthenticatedLayoutModule} from './shared/authenticated-layout/authenticated-layout.module';

const routes: Routes = [
    {
        path: '',
        component: AuthenticatedLayoutComponent,
        children: [
            {
                path: '',
                pathMatch: 'full',
                loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
            },
        ]
    }
];

@NgModule({
    imports: [
        AuthenticatedLayoutModule,
        TranslateModule.forRoot({
            defaultLanguage: 'en-US',
            useDefaultLang: true
        }),
        NgRxMessageBusModule.forRoot(),
        MomentModule.forRoot(),
        RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload', enableTracing: false}),
        ResolveModule
    ],
    providers: [
        DashboardGuard,
        WINDOW_PROVIDERS
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {
}
