import {Component, Inject, OnDestroy, OnInit} from '@angular/core';

import {
    NavigationCancel,
    NavigationEnd, NavigationError,
    NavigationStart,
    Router, RouterEvent,
} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {INgRxMessageBusService, MESSAGE_BUS_SERVICE_PROVIDER} from 'ngrx-message-bus';
import {interval, Subscription, timer} from 'rxjs';
import {AUTHENTICATION_SERVICE_INJECTOR, IPC_MESSAGE_HANDLER_PROVIDER} from '../constants/injection-token.constant';
import {IAuthenticationService} from '../services/interfaces/authentication-service.interface';
import {filter, tap} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';
import {WINDOW} from '../services/implementations/window.service';
import {IIpcMessageHandler} from '../interfaces/ipc-message-handler.interface';
import {IpcRenderer} from 'electron';
import {IpcMessageChannelConstant} from '../constants/ipc-message-channel.constant';
import {HubConnectionBuilder} from '@microsoft/signalr';
import {MeasurementDoneChannelEvent} from '../models/channel-events/measurement-done.channel-event';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'body',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

    //#region Properties

    // Subscription watch list.
    // tslint:disable-next-line:variable-name
    private readonly _subscription: Subscription;

    // Application pending progress counter which determine whether loader should be displayed or not.
    // tslint:disable-next-line:variable-name
    private _applicationPendingProgresses = 0;

    // Number of navigation processes.
    // tslint:disable-next-line:variable-name
    private _navigationProcesses = 0;

    // Window message listener.
    // tslint:disable-next-line:variable-name
    private _windowMessageListener: (message: MessageEvent) => any;

    //#endregion

    //#region Accessors

    // Navigation processes.
    public set navigationProcesses(value: number) {
        if (value < 0) {
            this._navigationProcesses = 0;
            return;
        }

        this._navigationProcesses = value;
    }

    public get navigationProcesses(): number {
        if (this._navigationProcesses < 0) {
            this._navigationProcesses = 0;
        }

        return this._navigationProcesses;
    }

    public get shouldLoaderDisplayed(): boolean {
        return (this._applicationPendingProgresses > 0);
    }

    //#endregion

    //#region Constructor

    constructor(private router: Router,
                private titleService: Title,
                protected translateService: TranslateService,
                @Inject(MESSAGE_BUS_SERVICE_PROVIDER) protected messageBusService: INgRxMessageBusService,
                @Inject(AUTHENTICATION_SERVICE_INJECTOR) protected authenticationService: IAuthenticationService,
                @Inject(IPC_MESSAGE_HANDLER_PROVIDER) protected ipcMessageHandlers: IIpcMessageHandler[],
                @Inject(WINDOW) protected window: Window) {

        this._subscription = new Subscription();
    }

    //#endregion

    //#region Methods

    // Called when component is initialized.
    public ngOnInit() {
        this.titleService.setTitle('Smart sensor');
    }

    // Called when component is destroyed.
    public ngOnDestroy(): void {
        if (this._subscription && !this._subscription.closed) {
            this._subscription.unsubscribe();
        }

        this.authenticationService
            .uninstallRefreshTokenTimer();

        if (this._windowMessageListener) {
            this.window.removeEventListener('message', this._windowMessageListener);
            this._windowMessageListener = null;
        }
    }

    // Get application title.
    public getTitle(): string {
        return this.titleService.getTitle();
    }

    //#endregion
}
