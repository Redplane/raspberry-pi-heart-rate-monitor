import {AppSettingsService} from '../services/implementations/app-settings.service';
import {AppSettings} from '../models/app-settings.model';

/*
* Load app settings asynchronously.
* */
export function loadAppSettingsAsync(appSettingsService: AppSettingsService): () => Promise<AppSettings> {

  console.log(navigator.userAgent);
  return () => appSettingsService
    .getAppSettingsAsync()
    .toPromise();
}
