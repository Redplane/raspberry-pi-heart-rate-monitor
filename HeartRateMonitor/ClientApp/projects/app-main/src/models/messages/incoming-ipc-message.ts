import {IIpcMessage} from '../../interfaces/ipc-message.interface';

export class IncomingIpcMessage<T> implements IIpcMessage {

    //#region Properties

    public readonly type: string;

    public readonly data: T;

    //#endregion

    //#region Constructor

    public constructor(type: string, data: T) {
        this.type = type;
        this.data = data;
    }

    //#endregion
}
