import {MeasurementViewModel} from '../../view-models/measurements/measurement.view-model';

export class MeasurementDoneMessage {

    //#region Constructor

    public constructor(public readonly measurements: MeasurementViewModel[],
                       public readonly time: string) {
    }

    //#endregion

}
