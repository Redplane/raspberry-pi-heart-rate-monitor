export class UserManagementRouteParam {

    //#region Properties

    public q: string;

    public page: number;

    //#endregion

}
