import {IMeasurementFactor} from '../interfaces/measurement-factor.interface';

export class MeasurementFactor<T> implements IMeasurementFactor<T> {

    //#region Properties

    public name: string;

    public title: string;

    public value: T;

    public unit?: string;

    public min?: number;

    public max?: number;

    //#endregion

    //#region Constructor

    public constructor(name: string, title: string, value?: T, unit?: string) {
        this.name = name;
        this.title = title;
        this.value = value;
        this.unit = unit;
    }

    //#endregion

}
