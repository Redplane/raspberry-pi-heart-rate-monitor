export abstract class TypedSessionRequest<T> {

    //#region Properties

    // Key to store session data.
    public abstract readonly key: string;

    //#endregion
}
