export class AccessTokenBeInFutureException {

    //#region Methods

    public constructor(public timeToRefresh?: number) {
    }

    //#endregion

}
