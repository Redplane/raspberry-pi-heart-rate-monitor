import {UrlTree} from '@angular/router';

export class BreadcrumbItem {

    //#region Properties

    public title: string;

    public link: UrlTree;

    public enabled: boolean;

    //#endregion

    //#region Constructor

    public constructor(title: string, link: UrlTree, enabled: boolean) {
        this.title = title;
        this.link = link;
        this.enabled = enabled;
    }

    //#endregion
}
