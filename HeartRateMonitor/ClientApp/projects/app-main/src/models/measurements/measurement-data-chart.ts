import {ChartDataSets, ChartOptions} from 'chart.js';
import {Color, Label} from 'ng2-charts';

export class MeasurementDataChart {

    public dataSets: ChartDataSets[];

    public options: ChartOptions;

    public labels: Label[];

    public colors: Color[];

    public legend: boolean;

    public type: string;

    //#region Constructor

    public constructor() {
        this.labels = [];
    }

    //#endregion

}
