export class Coordinate {

    //#region Properties

    public latitude: number;

    public longitude: number;

    //#endregion
}
