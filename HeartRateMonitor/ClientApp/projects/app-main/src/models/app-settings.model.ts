export class AppSettings {

    //#region Properties

    public appName: string;

    public clientId: string;

    public baseUrl: string;

    public apiBaseUrl: string;

    public photoUrl: string;

    //#endregion

}
