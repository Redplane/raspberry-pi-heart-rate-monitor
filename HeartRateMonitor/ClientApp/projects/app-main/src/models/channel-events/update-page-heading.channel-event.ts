import {TypedChannelEvent} from 'ngrx-message-bus';
import {MessageChannelNameConstant} from '../../constants/message-channel-name.constant';
import {MessageEventNameConstant} from '../../constants/message-event-name.constant';

export class UpdatePageHeadingChannelEvent extends TypedChannelEvent<string> {

    //#region Properties

    public readonly channelName: string;

    public readonly eventName: string;

    //#endregion

    //#region Constructor

    public constructor() {
        super();

        this.channelName = MessageChannelNameConstant.ui;
        this.eventName = MessageEventNameConstant.updatePageHeading;
    }

    //#endregion
}
