import {TypedChannelEvent} from 'ngrx-message-bus';
import {MessageChannelNameConstant} from '../../constants/message-channel-name.constant';
import {MessageEventNameConstant} from '../../constants/message-event-name.constant';
import {MeasurementDoneMessage} from '../messages/measurement-done-message';

export class MeasurementDoneChannelEvent
    extends TypedChannelEvent<MeasurementDoneMessage> {

    //#region Properties

    public readonly channelName: string;

    public readonly eventName: string;

    //#endregion

    //#region Constructor

    public constructor() {
        super();
        this.channelName = MessageChannelNameConstant.measurement;
        this.eventName = MessageEventNameConstant.didMeasurement;
    }

    //#endregion

}
