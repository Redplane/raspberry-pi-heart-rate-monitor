export class SelectableItem<T> {

    //#region Properties

    public readonly value: T;

    public readonly title: string;

    //#endregion

    //#region Constructor

    public constructor(title: string, value: T) {
        this.title = title;
        this.value = value;
    }

    //#endregion

}
