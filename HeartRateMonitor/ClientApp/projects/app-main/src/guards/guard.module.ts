import {NgModule} from '@angular/core';
import {AuthenticatedUserGuard} from './authenticated-user.guard';

@NgModule({
    providers: [
        AuthenticatedUserGuard
    ]
})
export class GuardModule {

}
