import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Inject, Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {NAVIGATION_SERVICE_PROVIDER, PLATFORM_SERVICE_PROVIDER} from '../constants/injection-token.constant';
import {IPlatformService} from '../services/interfaces/platform-service.interface';
import {Platforms} from '../enums/platforms.enum';
import {catchError, map, tap} from 'rxjs/operators';
import {ExceptionCodeConstant} from '../constants/exception-code.constant';
import {INavigationService} from '../services/interfaces/navigation-service.interface';
import {ScreenCodeConstant} from '../constants/screen-code.constant';

@Injectable()
export class DashboardGuard implements CanActivate {

    //#region Constructor

    public constructor(@Inject(PLATFORM_SERVICE_PROVIDER) protected platformService: IPlatformService,
                       @Inject(NAVIGATION_SERVICE_PROVIDER) protected navigationService: INavigationService) {
    }

    //#endregion

    //#region Methods

    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree>
        | Promise<boolean | UrlTree> | boolean | UrlTree {

        const getPlatformObservable = this.platformService.getPlatformAsync()
            .pipe(
                tap((platform: Platforms) => {
                    if (platform !== Platforms.web) {
                        throw new Error(ExceptionCodeConstant.platformIsNotWeb);
                    }
                })
            );

        return getPlatformObservable
            .pipe(
                map(_ => true),
                catchError(error => {
                    return of(this.navigationService.buildUrlTree(ScreenCodeConstant.deviceMeasurement, {}));
                })
            );
    }

    //#endregion
}
