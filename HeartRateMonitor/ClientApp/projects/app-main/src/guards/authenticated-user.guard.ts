import {
    ActivatedRouteSnapshot,
    CanActivate,
    CanLoad,
    Route, Router,
    RouterStateSnapshot,
    UrlSegment,
    UrlTree
} from '@angular/router';
import {Observable, of} from 'rxjs';
import {Inject, Injectable} from '@angular/core';
import {catchError, flatMap, map} from 'rxjs/operators';
import {IAuthenticationService} from '../services/interfaces/authentication-service.interface';
import {INavigationService} from '../services/interfaces/navigation-service.interface';
import {AUTHENTICATION_SERVICE_INJECTOR, NAVIGATION_SERVICE_PROVIDER} from '../constants/injection-token.constant';
import {ScreenCodeConstant} from '../constants/screen-code.constant';

@Injectable()
export class AuthenticatedUserGuard implements CanActivate, CanLoad {

    //#region Constructor

    public constructor(@Inject(AUTHENTICATION_SERVICE_INJECTOR) protected authenticationService: IAuthenticationService,
                       @Inject(NAVIGATION_SERVICE_PROVIDER) protected navigationService: INavigationService,
                       protected router: Router) {

    }

    //#endregion

    //#region Methods

    // Whether user can access to authenticated modules or not.
    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
        Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

        return this.authenticationService.loadProfileAsync()
            .pipe(
                map(_ => true),
                catchError(_ => {
                    const loginUrlTree = this.navigationService.buildUrlTree(ScreenCodeConstant.login, {});
                    return of(loginUrlTree);
                })
            );

    }

    /*
    * Whether module can be loaded or not.
    * */
    public canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
        return this.authenticationService.loadProfileAsync()
            .pipe(
                map(_ => true),
                catchError(_ => {
                    return of(false);
                })
            );
    }
    //#endregion
}
