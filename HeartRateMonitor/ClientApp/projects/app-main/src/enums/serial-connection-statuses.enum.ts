export enum SerialPortConnectionStatuses {
    unknown,
    opened,
    closed
}
