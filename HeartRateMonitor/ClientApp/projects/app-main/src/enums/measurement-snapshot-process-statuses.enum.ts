export enum MeasurementSnapshotProcessStatuses {
    unknown,
    stopped,
    running
}
