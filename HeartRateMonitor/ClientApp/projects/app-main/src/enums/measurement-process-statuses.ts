export enum MeasurementProcessStatuses {
    unavailable,
    running,
    stopped
}
