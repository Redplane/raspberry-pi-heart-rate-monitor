export enum MeasurementDataTypes {
    byte,
    shortByte,
    short,
    unsignedShort,
    integer,
    unsignedInteger,
    long,
    unsignedLong,
    float,
    double,
    decimal
}
