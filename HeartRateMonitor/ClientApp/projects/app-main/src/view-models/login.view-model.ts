export class LoginViewModel {

  //#region Properties

  public domain: number;

  public username: string;

  public password: string;

  //#endregion

}
