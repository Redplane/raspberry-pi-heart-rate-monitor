export class InvalidBusinessResponseViewModel {

  //#region Properties

  public constructor(public readonly code: string,
                     public readonly message?: string,
                     public readonly additionalData?: any) {
  }

  //#endregion

}
