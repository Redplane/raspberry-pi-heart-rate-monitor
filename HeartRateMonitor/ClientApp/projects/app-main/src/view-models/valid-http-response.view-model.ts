import {HttpResponseViewModel} from './http-response.view-model';

export class ValidHttpResponseViewModel<T> extends HttpResponseViewModel {

    //#region Constructor

    constructor(status: number, public data: T) {
        super(status);
    }

    //#endregion

}
