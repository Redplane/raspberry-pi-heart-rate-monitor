import {IUser} from '../../interfaces/user.interface';
import {UserStatuses} from '../../enums/user-status.enum';

export class ProfileViewModel implements IUser {

    //#region Properties

    public email: string;

    public id: string | number;

    public username: string;

    public firstName: string;

    public lastName: string;

    public roles: string[];

    public avatar: string;

    public status: UserStatuses;

    public createdDate: string;

    public updatedDate: string;

    //#endregion

    //#region Constructor

    public constructor() {
        this.roles = [];
        this.status = UserStatuses.disable;
    }

    //#endregion
}
