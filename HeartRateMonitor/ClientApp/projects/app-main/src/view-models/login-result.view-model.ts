import {UserViewModel} from './user.view-model';
import {ProfileViewModel} from './user/profile.view-model';

export class LoginResultViewModel {

    //#region Properties

    // Token that is used for accessing to protected sources in system.
    public accessToken: string;

    // Token which is used for refreshing the access token.
    public refreshToken: string;

    // How many seconds the token can live.
    public lifeTime: number;

    /* The number of seconds from 1970 until token is issued*/
    public issuedAt: number;

    /* Current user profile*/
    public user: ProfileViewModel;

    // Whether youtube account is authenticated or not.
    public authenticatedYoutube: boolean;

    //#endregion

}
