import {SerialPortSettings} from '@app/common';
import {AddSerialPortViewModel} from '../connection/add-serial-port.view-model';
import {EditSerialPortViewModel} from '../connection/edit-serial-port.view-model';

export class SetupInOutPortViewModel extends SerialPortSettings {

    //#region Properties

    //#endregion

    //#region Constructor

    public constructor(settings: AddSerialPortViewModel | EditSerialPortViewModel) {

        super();

        if (settings instanceof AddSerialPortViewModel) {
            const addSerialPortCommand = settings as AddSerialPortViewModel;
            this.name = addSerialPortCommand.name;
            this.baudRate = addSerialPortCommand.baudRate;
            this.dataBit = addSerialPortCommand.dataBit;
            this.parity = addSerialPortCommand.parity;
            this.stopBits = addSerialPortCommand.stopBits;
        } else {
            const editSerialPortCommand = settings as EditSerialPortViewModel;
            this.name = editSerialPortCommand.name.value;
            this.baudRate = editSerialPortCommand.baudRate.value;
            this.dataBit = editSerialPortCommand.dataBit.value;
            this.parity = editSerialPortCommand.parity.value;
            this.stopBits = editSerialPortCommand.stopBit.value;
        }
    }

    //#endregion

}
