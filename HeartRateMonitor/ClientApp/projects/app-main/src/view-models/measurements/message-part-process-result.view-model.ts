import {MeasurementViewModel} from './measurement.view-model';

export class MessagePartProcessResultViewModel {
    public data: MeasurementViewModel;

    public success: boolean;
}
