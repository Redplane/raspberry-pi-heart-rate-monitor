export class MeasurementViewModel {

    //#region Constructor

    public constructor(public readonly name: string,
                       public value: number) {
    }

    //#endregion

}
