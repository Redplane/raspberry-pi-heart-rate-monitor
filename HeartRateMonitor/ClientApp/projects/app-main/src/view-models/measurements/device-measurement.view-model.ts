import {MeasurementViewModel} from './measurement.view-model';

export class DeviceMeasurementViewModel {

    //#region Constructor

    public constructor(public readonly measurements: MeasurementViewModel[],
                       public readonly time: Date) {
    }

    //#endregion

}
