import {EditableField} from '@app/common';

export class EditSerialPortViewModel {

    //#region Properties

    public name: EditableField<string>;

    public baudRate: EditableField<number>;

    public dataBit: EditableField<number>;

    public parity: EditableField<number>;

    public stopBit: EditableField<number>;

    public autoStart: EditableField<boolean>;

    //#endregion

    //#region Constructor

    public constructor(public readonly initialName: string) {
    }

    //#endregion

}
