export class GetConnectionsStatusViewModel {

    //#region Constructor

    public constructor(public connectionIds: string[]) {
    }

    //#endregion

}
