import {IConnectionSettings} from '../../../../app-common/src/interfaces/connection-settings.interface';

export class ConnectionStatusViewModel {

    //#region Properties

    public name: string;

    public isConnected: boolean;

    public settings: IConnectionSettings;

    //#endregion

}
