import {EditableField} from '@app/common';

export class UpdateMeasurementProcessSettingsViewModel {

    //#region Properties

    public slaveId: EditableField<number>;

    public timeout: EditableField<number>;

    public interval: EditableField<number>;

    //#endregion

}
