import {HttpResponseViewModel} from './http-response.view-model';

export class InvalidHttpResponseViewModel extends HttpResponseViewModel {

    //#region Constructor

    public constructor(status: number, public code: string, public message: string) {
        super(status);
    }

    //#endregion

}
