export class MeasurementProcessSettingsViewModel {

    //#region Properties

    public timeout?: number;

    public interval: number;

    public slaveId: number;

    //#endregion

    //#region Constructor

    public constructor() {
        this.slaveId = 1;
    }

    //#endregion
}
