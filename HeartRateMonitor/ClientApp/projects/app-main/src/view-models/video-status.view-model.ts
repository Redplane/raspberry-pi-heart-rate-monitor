export class VideoStatusViewModel {

    //#region Properties

    // tslint:disable-next-line:variable-name
    private readonly _id: string;

    // tslint:disable-next-line:variable-name
    private readonly _description: string;

    //#endregion

    //#region Accessors

    public get id(): string {
        return this._id;
    }

    public get description(): string {
        return this._description;
    }

    //#endregion

    //#region Constructor

    public constructor(id: string, description: string) {
        this._id = id;
        this._description = description;
    }

    //#endregion

}
