export class SearchResultViewModel<T> {

    //#region Properties

    public totalRecords: number;

    public items: T[];

    //#endregion

    //#region Constructor

    public constructor(items?: T[], totalRecords?: number) {
        this.totalRecords = totalRecords || 0;
        this.items = items || [];
    }

    //#endregion

}
