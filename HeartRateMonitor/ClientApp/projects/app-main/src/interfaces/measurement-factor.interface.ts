export interface IMeasurementFactor<T> {

    //#region Properties

    name: string;

    title: string;

    value: T;

    //#endregion

}
