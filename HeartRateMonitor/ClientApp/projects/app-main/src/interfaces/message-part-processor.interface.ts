import {MeasurementDataTypes} from '../enums/measurement-data-types.enum';

export interface IMessagePartProcessor {

    //#region Properties

    // Name of message processor to which message part processor is attached to.
    host: string;

    // Name of parameter.
    name: string;

    // Measurement unit.
    unit: string;

    // Minimum measurements value.
    min: number;

    // Maximum measurements value.
    max: number;

    // Measurement data type.
    measurementDataTypes: MeasurementDataTypes;

    //#endregion

}
