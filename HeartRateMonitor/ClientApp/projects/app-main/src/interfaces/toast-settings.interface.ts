export interface IToastSettings {

    //#region Properties

    // The millisecond the toast notification is delayed.
    delay: number;

    // Duration of toast message.
    duration: number;

    //#endregion

}
