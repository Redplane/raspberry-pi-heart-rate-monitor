export interface IIpcMessage {

    //#region Properties

    // Type of ipc message.
    readonly type: string;

    //#endregion
}
