export interface IUser {

  //#region Properties

  id: string | number;

  email: string;

  username: string;

  //#endregion
}
