export interface IIpcMessageHandler {

    //#region Methods

    // Process ipc message.
    processMessage(message: MessageEvent): void;
    
    //#endregion

}
