export interface IWidget {

    //#region Properties

    icon: string;

    title: string;

    value: string;

    type: 'primary' | 'secondary' | 'warning' | 'danger' | 'success' | 'info';

    //#endregion

}
