import {StorageMap} from '@ngx-pwa/local-storage';
import {Injectable} from '@angular/core';

@Injectable()
export class StorageMapMock extends StorageMap {
}
