export class SerialPortSettings {

    //#region Properties

    name: string;

    baudRate: number;

    dataBit: number;

    parity: number;

    stopBits: number;

    autoStart: boolean;

    //#endregion

}
