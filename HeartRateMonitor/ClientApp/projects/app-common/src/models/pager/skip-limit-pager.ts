import {PagerKindsConstant} from '../../constants/pager-kinds.constant';
import {IPager} from '../../../../app-main/src/interfaces/pager.interface';

export class SkipLimitPager implements IPager {

    //#region Properties

    public readonly kind = PagerKindsConstant.skipLimit;

    //#endregion

    //#region Constructor

    public constructor(public totalRecords: number, public skip: number) {

    }

    //#endregion
}

