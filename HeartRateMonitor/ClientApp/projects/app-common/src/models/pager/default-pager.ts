import {PagerKindsConstant} from '../../constants/pager-kinds.constant';
import {IPager} from '../../../../app-main/src/interfaces/pager.interface';

export class DefaultPager implements IPager {

    //#region Properties

    public readonly kind = PagerKindsConstant.default;

    //#endregion

    //#region Constructor

    public constructor(public page: number, public totalRecords: number) {

    }

    //#endregion
}
