export class PagerKindsConstant {

    //#region Properties

    public static readonly default = 'default';

    public static readonly skipLimit = 'skip-limit';

    //#endregion

}
