# XLINK REPORTER

A small project about sensor reader.

## Project structure.
```
.
+-- src
|   +-- assets
|   +-- constants
|   +-- environments
|   +-- factories
|   +-- guards
|   +-- interceptors
|   +-- interfaces
|   +-- models
|   +-- modules
|       +-- <module a>
|           +-- <module a>.component.html
|           +-- <module a>.component.ts
|       +-- <module b>
|           +-- <module b>.component.html
|           +-- <module b>.component.ts
|       <parent module>.module.ts
|       <parent module>.module.ts
+-- index.html (Index file)
```

## Structure description.
- ```assets``` : Static files (such as: ```*.css```, ```*.json```, ...) should be stored in this folder.
- ```constants```: Appliation constant files should be stored in this folder (such as: ```app-settings.constant.ts```, ```urlStates.constant.ts```, ...). Naming convention: ```*.constants.ts```.
- ```environments```: Contains classes or variables that depend on build environment.
- ```factories```: Contains application factory functions.
- ```interceptors```: Contains application interceptor.
- ```modules```: Application main modules should be stored in this folder
- ```<module a>```: Folder contains module a definition. Such as : ```account```, ```category```, ...
- ```<module a>.component.html```: Template file for ```module a```.
- ```<module a>.component.ts```: Logic definition file of ```module a```.
- ```<parent module>.module.ts```: Importing/exporting children modules. For example: ```user-management```, ```category-management```.
- ```<parent module>.route.ts```: Child modules' routes definition.
- ```interfaces```: Contains application interfaces which is for classes to implement.
- ```models```: Contains application model files, mostly about database entities.
- ```modules```: Contains application modules files.
- ```services```: Contains application services to inject to components.
- ```view-models```: Contains view-model files that are bridge between api end-point and application.
- ```index.html```: Application entry html file.

## Project commnands.
- `ng serve`: Bundle and publish project files. Files which are generated will be placed inside `dist` folder.
- `npm build --prod`: Bundle and start watching project. This is for development stage. Browser will be reloaded automatically when changes are detected.
