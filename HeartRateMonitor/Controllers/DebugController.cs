using System;
using System.Threading.Tasks;
using HeartRateMonitor.Hubs;
using HeartRateMonitor.Models;
using HeartRateMonitor.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace HeartRateMonitor.Controllers
{
    [Route("api/debug")]
    public class DebugController : Controller
    {
        #region Properties

        private readonly IHubContext<MeasurementHub> _measurementHubContext;

        private readonly IEcgService _ecgValueService;
        
        #endregion
        
        #region Constructor

        public DebugController(IHubContext<MeasurementHub> measurementHubContext, IEcgService ecgValueValueService)
        {
            _measurementHubContext = measurementHubContext;
            _ecgValueService = ecgValueValueService;
        }

        #endregion
        
        #region Methods

        [HttpPost("measurement-message")]
        public virtual async Task<IActionResult> SendMeasurementAsync()
        {
            var measurement = new EcgMeasurement(DateTime.UtcNow, (int)10);
            //var mainWindow = Electron.WindowManager.BrowserWindows.FirstOrDefault();
            //Electron.IpcMain.Send(mainWindow, "ecg", measurement);
            await _measurementHubContext.Clients.All.SendAsync("OnMeasurementDone", measurement);

            _ecgValueService.PublishMeasurementToIotHub(DateTime.Now, (int)10);
            return Ok();
        }
        
        #endregion
    }
}