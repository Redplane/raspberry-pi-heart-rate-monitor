﻿using System;
using System.Net;
using System.Threading.Tasks;
using HeartRateMonitor.Cqrs.Commands;
using HeartRateMonitor.Cqrs.Queries;
using HeartRateMonitor.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace HeartRateMonitor.Controllers
{
    [Route("api/serial-port")]
    public class SerialPortController : Controller
    {
        #region Properties

        private readonly IMediator _mediator;

        #endregion

        #region Constructor

        public SerialPortController(IServiceProvider serviceProvider)
        {
            _mediator = serviceProvider.GetService<IMediator>();
        }

        #endregion

        #region Methods

        [HttpGet("available-ports")]
        public async Task<IActionResult> GetAvailableSerialPortsAsync()
        {
            var query = new GetAvailableSerialPortsQuery();
            var availablePorts = await _mediator.Send(query);
            return Ok(availablePorts);
        }

        [HttpGet("settings")]
        public async Task<IActionResult> GetInOutPortSettingsAsync()
        {

            var request = new GetSerialPortSettingsQuery();
            var settings = await _mediator.Send(request);

            return Ok(settings);
        }

        [HttpPost("")]
        public virtual async Task<IActionResult> SetupIoPortAsync([FromBody] SetupSerialPortCommand command)
        {
            var settings = await _mediator.Send(command);

            if (settings == null)
                return NotFound();

            return Ok(settings);
        }

        #endregion
    }
}