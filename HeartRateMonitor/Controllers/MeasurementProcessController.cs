﻿using System;
using System.Threading.Tasks;
using HeartRateMonitor.Cqrs.Commands;
using HeartRateMonitor.Cqrs.Queries;
using HeartRateMonitor.Cqrs.QueryHandler;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace HeartRateMonitor.Controllers
{
	[Route("api/measurement-process")]
	public class MeasurementProcessController : Controller
	{
		#region Properties

		private readonly IMediator _mediator;

		#endregion

		#region Constructor

		public MeasurementProcessController(IServiceProvider serviceProvider)
		{
			_mediator = serviceProvider.GetService<IMediator>();
		}

		#endregion

		#region Methods

		[HttpGet("status")]
		public virtual async Task<IActionResult> GetMeasurementStatusAsync()
		{
			var status = await _mediator.Send(new GetMeasurementProcessStatusQuery());
			return Ok(status);
		}

		[HttpGet]
		public virtual async Task<IActionResult> GetMeasurementProcessAsync()
		{
			var request = new GetMeasurementProcessSettingsQuery();
			var measurementSettings = await _mediator.Send(request);
			return Ok(measurementSettings);
		}

		[HttpPut]
		public virtual async Task<IActionResult> EditMeasurementProcessAsync([FromBody] EditMeasurementProcessCommand command)
		{
			var measurementSettings = await _mediator.Send(command);
			return Ok(measurementSettings);
		}

		[HttpPost]
		public virtual async Task<IActionResult> StartMeasurementProcessAsync()
		{
			var command = new StartMeasurementProcessCommand();
			await _mediator.Send(command);
			return Ok();
		}

		[HttpDelete]
		public virtual async Task<IActionResult> StopMeasurementProcessAsync()
		{
			var command = new StopMeasurementProcessCommand();
			await _mediator.Send(command);
			return Ok();
		}

		#endregion
	}
}