﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using ElectronNET.API;
using HeartRateMonitor.Constants;
using HeartRateMonitor.Enums;
using HeartRateMonitor.Hubs;
using HeartRateMonitor.Models;
using HeartRateMonitor.Models.Messages;
using HeartRateMonitor.Services.Interfaces;
using LiteMessageBus.Services.Interfaces;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NModbus;
using NModbus.Serial;
using NModbus.Utility;

namespace HeartRateMonitor.HostedServices
{
	public class MeasurementHostedService : IHostedService
	{
		#region Properties

		private readonly IServiceProvider _serviceProvider;

		private readonly LinkedList<IDisposable> _subscriptions;

		#endregion

		#region Constructor

		public MeasurementHostedService(IServiceProvider serviceProvider)
		{
			_serviceProvider = serviceProvider;
			_subscriptions = new LinkedList<IDisposable>();
		}

		#endregion

		#region Methods

		public virtual Task StartAsync(CancellationToken cancellationToken)
		{
			using var serviceScope = _serviceProvider.CreateScope();
			var messageBusService = serviceScope.ServiceProvider.GetService<ILiteMessageBusService>();

			// Hook event about measurement process started.
			var hookMeasurementProcessStartedSubscription = messageBusService
				.HookMessageChannel<MeasurementProcessStartedMessage>(MessageChannelNames.MeasurementProcess,
					MessageEventNames.MeasurementProcessStarted)
				.ObserveOn(ThreadPoolScheduler.Instance)
				.Subscribe(async message => await OnMeasurementProcessStarted(message));

			var hookSerialPortConnectionStatusChangedSubscription = messageBusService
				.HookMessageChannel<SerialPortConnectionStatusChangedMessage>(MessageChannelNames.Connection,
					MessageEventNames.SerialPortConnectionStatusChanged)
				.ObserveOn(ThreadPoolScheduler.Instance)
				.Subscribe(async message => await OnSerialPortConnectionStatusChanged(message));

			_subscriptions.AddLast(hookMeasurementProcessStartedSubscription);
			_subscriptions.AddLast(hookSerialPortConnectionStatusChangedSubscription);

			return Task.CompletedTask;
		}

		public virtual Task StopAsync(CancellationToken cancellationToken)
		{
			if (_subscriptions != null)
			{
				foreach (var subscription in _subscriptions)
					subscription.Dispose();
			}

			return Task.CompletedTask;
		}

		#endregion

		#region Internal methods

		protected virtual async Task OnMeasurementProcessStarted(MeasurementProcessStartedMessage message)
		{
			using var serviceScope = _serviceProvider.CreateScope();
			var serialPortService = serviceScope.ServiceProvider.GetService<ISerialPortService>();
			var measurementProcessService =
				serviceScope.ServiceProvider.GetService<IMeasurementProcessService>();
			var measurementHub = _serviceProvider.GetService<IHubContext<MeasurementHub>>();


			// Get the current measurement process settings.
			var measurementProcessSettings = await measurementProcessService.GetSettingsAsync();

			// Get modbus master.
			var modbusMaster = serialPortService.GetModBusRtuMaster();

			if (modbusMaster == null)
				return;

			if (!measurementProcessSettings.HasValidSlaveId())
				return;

			var egcValueService = serviceScope.ServiceProvider.GetService<IEcgService>();
			var measurementService = serviceScope.ServiceProvider.GetService<IMeasurementService>();

			measurementProcessService.Start(async () =>
			{
				//var value = random.Next(0, 100);

				//var adapter = new SerialPortAdapter(serialPortService.GetSerialPort());
				//var factory = new ModbusFactory();
				//var modbusMaster = factory.CreateRtuMaster(adapter);

				try
				{
					modbusMaster.Transport.ReadTimeout = 1000;
					var data = await modbusMaster.ReadHoldingRegistersAsync((byte)measurementProcessSettings.SlaveId, 0, 2);
					var value = ModbusUtility.GetSingle(data[1], data[0]);

					var actualValue = (value / 4095.0) * 100;
					if (actualValue > 100)
						return;

					if (actualValue < 100)
						measurementService.SaveMaxMeasurementValue((int) actualValue);
					
					var measurement = new EcgMeasurement(DateTime.UtcNow, (int)actualValue);
					//var mainWindow = Electron.WindowManager.BrowserWindows.FirstOrDefault();
					//Electron.IpcMain.Send(mainWindow, "ecg", measurement);
					await measurementHub.Clients.All.SendAsync("OnMeasurementDone", measurement);

					egcValueService.PublishMeasurementToIotHub(DateTime.Now, (int)actualValue);

					// TODO: Implement measurement.
				}
				catch
				{
					
				}
				

			}, measurementProcessSettings);
		}

		protected virtual Task OnSerialPortConnectionStatusChanged(SerialPortConnectionStatusChangedMessage message)
		{
			using var serviceScope = _serviceProvider.CreateScope();
			var serialPortService = serviceScope.ServiceProvider.GetService<ISerialPortService>();
			var measurementProcessService =
				serviceScope.ServiceProvider.GetService<IMeasurementProcessService>();

			if (message.Status != SerialPortConnectionStatuses.Connected)
				measurementProcessService.Stop();

			return Task.CompletedTask;
		}

		#endregion
	}
}