using System;
using System.IO;
using ElectronNET.API;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace HeartRateMonitor
{
    public class Program
    {
        #region Methods

        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
	        var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

	        var configuration = new ConfigurationBuilder()
		        .SetBasePath(Directory.GetCurrentDirectory())
		        .AddJsonFile("appsettings.json", false, true)
		        .AddJsonFile($"appsetting.{environment}.json", true, true)
		        .AddUserSecrets<Startup>(true)
		        .Build();

	        var logger = new LoggerConfiguration()
		        .ReadFrom.Configuration(configuration)
		        .CreateLogger();

            return Host.CreateDefaultBuilder(args)
		        .ConfigureWebHostDefaults(webBuilder =>
		        {
			        webBuilder.UseConfiguration(configuration);
					webBuilder.UseElectron(args);
			        webBuilder.UseStartup<Startup>();
		        });
        }

        #endregion
    }
}
