﻿namespace HeartRateMonitor.Constants
{
    public class MessageChannelNames
    {
        #region Properties

        public const string MeasurementProcess = "MEASUREMENT_PROCESS";

        public const string Connection = "CONNECTION";

        #endregion
	}
}