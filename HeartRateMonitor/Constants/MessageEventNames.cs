﻿namespace HeartRateMonitor.Constants
{
    public class MessageEventNames
    {
        #region Properties

        public const string MeasurementProcessStarted = "MEASUREMENT_STARTED";

        public const string SerialPortConnectionStatusChanged = "SERIAL_PORT_CONNECTION_STATUS_CHANGED";

        #endregion
    }
}