﻿namespace HeartRateMonitor.ViewModels
{
	public class EditableFieldViewModel<T>
	{
		#region Properties

		public T Value { get; set; }

		public bool HasModified { get; set; }

		#endregion

		#region Constructor

		public EditableFieldViewModel()
		{
		}

		public EditableFieldViewModel(T value, bool hasModified)
		{
			Value = value;
			HasModified = hasModified;
		}

		#endregion
	}
}