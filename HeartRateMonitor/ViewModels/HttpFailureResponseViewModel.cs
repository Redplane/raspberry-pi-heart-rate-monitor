﻿using System.Collections.Generic;
using HeartRateMonitor.Models;

namespace HeartRateMonitor.ViewModels
{
    public class HttpFailureResponseViewModel<T>
    {
        #region Properties

        public string Code { get; private set; }

        public string Message { get; private set; }

        public T AdditionalData { get; set; }

        #endregion

        #region Constructor

        public HttpFailureResponseViewModel(string code, string message)
        {
            Code = code;
            Message = message;
        }

        #endregion
    }

    public class HttpFailureResponseViewModel : HttpFailureResponseViewModel<object>
    {
        #region Constructor

        public HttpFailureResponseViewModel(string code, string message) : base(code, message)
        {
        }

        public HttpFailureResponseViewModel(HttpResponseException exception) : this(exception.Code, exception.Message)
        {
            AdditionalData = exception.AdditionalData;
        }

        #endregion
    }
}