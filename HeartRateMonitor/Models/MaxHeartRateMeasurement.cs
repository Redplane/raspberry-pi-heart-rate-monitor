﻿using System;
using Newtonsoft.Json;

namespace HeartRateMonitor.Models
{
	public class MaxHeartRateMeasurement
	{
		#region Properties


		[JsonProperty("recorded_date")]
		public DateTime Time { get; private set; }

		[JsonProperty("max_heartrate")]
		public int Value { get; private set; }

		[JsonProperty("id")]
		public Guid Id { get; set; }

		#endregion

		#region Constructor

		public MaxHeartRateMeasurement(DateTime time, int value)
		{
			Id = Guid.Parse("51864875-ca29-4bb2-9713-d1aca0deb2ce");
			Time = time;
			Value = value;
		}

		#endregion
    }
}