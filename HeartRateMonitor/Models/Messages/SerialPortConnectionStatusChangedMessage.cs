﻿using HeartRateMonitor.Enums;

namespace HeartRateMonitor.Models.Messages
{
	public class SerialPortConnectionStatusChangedMessage
	{
		#region Properties

		public SerialPortConnectionStatuses Status { get; private set; }

		#endregion

		#region Constructor

		public SerialPortConnectionStatusChangedMessage(SerialPortConnectionStatuses status)
		{
			Status = status;
		}

		#endregion
	}
}