﻿namespace HeartRateMonitor.Models.Messages
{
    public class MeasurementProcessStartedMessage
    {
        #region Properties

        public MeasurementProcessSettings Settings { get; private set; }

        #endregion

        #region Constructor

        public MeasurementProcessStartedMessage(MeasurementProcessSettings settings)
        {
            Settings = settings;
        }

        #endregion

        #region Methods

        #endregion
    }
}