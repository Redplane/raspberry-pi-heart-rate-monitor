﻿namespace HeartRateMonitor.Models
{
	public class MeasurementProcessSettings
	{
		#region Properties

		public int SlaveId { get; set; }

		public int Interval { get; set; }

		public int? Timeout { get; set; }

		#endregion

		#region Methods

		public static MeasurementProcessSettings GetDefaultSettings()
		{
			var settings = new MeasurementProcessSettings();
			settings.Timeout = -1;
			settings.Interval = 1000;

			return settings;
		}

		public virtual bool HasValidSlaveId()
		{
			return (0 <= SlaveId && SlaveId <= 255);
		}

		#endregion
	}
}