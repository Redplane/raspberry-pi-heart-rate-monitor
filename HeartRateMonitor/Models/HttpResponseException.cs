﻿using System;
using System.Net;

namespace HeartRateMonitor.Models
{
    public class HttpResponseException : Exception
    {
        #region Properties

        public HttpStatusCode StatusCode { get; private set; }

        public string Code { get; private set; }

        public object AdditionalData { get; set; }

        #endregion

        #region Constructor

        public HttpResponseException(HttpStatusCode statusCode,
            string code, string message) : base(message)
        {
            StatusCode = statusCode;
            Code = code;
        }

        public HttpResponseException(HttpStatusCode statusCode,
            string code, string message,
            object additionalData) : this(statusCode, code, message)
        {
            AdditionalData = additionalData;
        }

        #endregion
    }
}