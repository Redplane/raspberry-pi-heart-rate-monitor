using System.IO.Ports;

namespace HeartRateMonitor.Models
{
    public class SerialPortSettings
    {
        #region Properties

        /// <summary>
        /// Name of serial port.
        /// </summary>
        public string Name { get; protected set; }

        public int BaudRate { get; protected set; }

        public int DataBit { get; protected set; }

        public Parity Parity { get; protected set; }

        public StopBits StopBits { get; protected set; }

        public bool AutoStart { get; set; }

        #endregion

        #region Constructor

        public SerialPortSettings(string name, int baudRate, int dataBit, Parity parity, StopBits stopBits)
        {
            Name = name;
            BaudRate = baudRate;
            DataBit = dataBit;
            Parity = parity;
            StopBits = stopBits;
        }

        #endregion
    }
}
