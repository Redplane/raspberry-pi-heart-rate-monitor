﻿namespace HeartRateMonitor.Models
{
    public class IotHubSettings
    {
        #region Properties

        /// <summary>
        /// Hub endpoint.
        /// </summary>
        public string Endpoint { get; set; }

        /// <summary>
        /// Broker port.
        /// </summary>
        public int BrokerPort { get; set; }

        /// <summary>
        /// Root certificate
        /// </summary>
        public string RootCertificate { get; set; }

        public string ClientCertificate { get; set; }

        public string ClientCertificatePassword { get; set; }

        public string StartupUrl { get; set; }

        public string MeasurementTopic { get; set; }

        public string MaxHeartRateTopic { get; set; }

        #endregion
    }
}