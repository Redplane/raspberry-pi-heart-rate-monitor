﻿using System;
using Newtonsoft.Json;

namespace HeartRateMonitor.Models
{
	public class MaxEcgMeasurement
	{
		#region Properties


		[JsonProperty("recorded_date")]
		public DateTime Time { get; private set; }

		[JsonProperty("max_heartrate")]
		public int Value { get; private set; }

		[JsonProperty("patient_id")]
		public Guid PatientId { get; set; }

		#endregion

		#region Constructor

		public MaxEcgMeasurement(DateTime time, int value)
		{
			PatientId = Guid.Parse("51864875-ca29-4bb2-9713-d1aca0deb2ce");
			Time = time;
			Value = value;
		}

		#endregion
    }
}