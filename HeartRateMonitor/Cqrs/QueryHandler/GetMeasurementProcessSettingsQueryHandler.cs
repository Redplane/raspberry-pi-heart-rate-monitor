﻿using System;
using System.Threading;
using System.Threading.Tasks;
using HeartRateMonitor.Cqrs.Queries;
using HeartRateMonitor.Models;
using HeartRateMonitor.Services.Interfaces;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace HeartRateMonitor.Cqrs.QueryHandler
{
	public class GetMeasurementProcessSettingsQueryHandler : IRequestHandler<GetMeasurementProcessSettingsQuery, MeasurementProcessSettings>
	{
		#region Properties

		private readonly IMeasurementProcessService _measurementProcessService;

		#endregion

		#region Constructor

		public GetMeasurementProcessSettingsQueryHandler(IServiceProvider serviceProvider)
		{
			_measurementProcessService = serviceProvider.GetService<IMeasurementProcessService>();
		}

		#endregion

		#region Methods

		public virtual async Task<MeasurementProcessSettings> Handle(GetMeasurementProcessSettingsQuery request, CancellationToken cancellationToken)
		{
			var settings = await _measurementProcessService.GetSettingsAsync();
			return settings;
		}

		#endregion
	}
}