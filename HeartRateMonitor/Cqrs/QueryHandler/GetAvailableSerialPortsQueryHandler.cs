﻿using System;
using System.Threading;
using System.Threading.Tasks;
using HeartRateMonitor.Cqrs.Queries;
using HeartRateMonitor.Services.Interfaces;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace HeartRateMonitor.Cqrs.QueryHandler
{
    public class GetAvailableSerialPortsQueryHandler : IRequestHandler<GetAvailableSerialPortsQuery, string[]>
    {
        #region Properties

        // ReSharper disable once InconsistentNaming
        protected readonly ISerialPortService _serialPortService;

        #endregion

        #region Constructor

        public GetAvailableSerialPortsQueryHandler(IServiceProvider serviceProvider)
        {
            _serialPortService = serviceProvider.GetService<ISerialPortService>();
        }

        #endregion

        #region Methods

        public virtual async Task<string[]> Handle(GetAvailableSerialPortsQuery request, CancellationToken cancellationToken)
        {
            var availableSerialPorts = await _serialPortService.GetAvailablePortsAsync();
            return availableSerialPorts;

        }

        #endregion
    }
}