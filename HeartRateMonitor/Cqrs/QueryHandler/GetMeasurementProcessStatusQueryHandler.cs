﻿using System.Threading;
using System.Threading.Tasks;
using HeartRateMonitor.Cqrs.Queries;
using HeartRateMonitor.Enums;
using HeartRateMonitor.Services.Interfaces;
using MediatR;

namespace HeartRateMonitor.Cqrs.QueryHandler
{
	public class GetMeasurementProcessStatusQueryHandler : IRequestHandler<GetMeasurementProcessStatusQuery, MeasurementProcessStatuses>
	{
		#region Properties

		private readonly IMeasurementProcessService _measurementProcessService;

		#endregion

		#region Constructor

		public GetMeasurementProcessStatusQueryHandler(IMeasurementProcessService measurementProcessService)
		{
			_measurementProcessService = measurementProcessService;
		}

		#endregion

		#region Methods

		public virtual Task<MeasurementProcessStatuses> Handle(GetMeasurementProcessStatusQuery request, CancellationToken cancellationToken)
		{
			var status = _measurementProcessService.GetStatus();
			return Task.FromResult(status);
		}

		#endregion
	}
}