﻿using System;
using System.Threading;
using System.Threading.Tasks;
using HeartRateMonitor.Cqrs.Queries;
using HeartRateMonitor.Models;
using HeartRateMonitor.Services.Interfaces;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace HeartRateMonitor.Cqrs.QueryHandler
{
	public class GetSerialPortSettingsQueryHandler : IRequestHandler<GetSerialPortSettingsQuery, SerialPortSettings>
	{
		#region Properties

		protected readonly ISerialPortService _serialPortService;

		#endregion

		#region Constructor

		public GetSerialPortSettingsQueryHandler(IServiceProvider serviceProvider)
		{
			_serialPortService = serviceProvider.GetService<ISerialPortService>();
		}

		#endregion

		#region Methods

		public virtual Task<SerialPortSettings> Handle(GetSerialPortSettingsQuery request, CancellationToken cancellationToken)
		{
			var settings = _serialPortService.GetSettings();
			return Task.FromResult(settings);
		}

		#endregion
	}
}