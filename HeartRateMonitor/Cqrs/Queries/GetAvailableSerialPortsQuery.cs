﻿using MediatR;

namespace HeartRateMonitor.Cqrs.Queries
{
    public class GetAvailableSerialPortsQuery : IRequest<string[]>
    {
        
    }
}