﻿using HeartRateMonitor.Enums;
using MediatR;

namespace HeartRateMonitor.Cqrs.Queries
{
	public class GetMeasurementProcessStatusQuery : IRequest<MeasurementProcessStatuses>
	{
		
	}
}