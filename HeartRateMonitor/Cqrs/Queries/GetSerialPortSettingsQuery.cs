﻿using HeartRateMonitor.Models;
using MediatR;

namespace HeartRateMonitor.Cqrs.Queries
{
    public class GetSerialPortSettingsQuery : IRequest<SerialPortSettings>
    {
        
    }
}