﻿using HeartRateMonitor.Models;
using MediatR;

namespace HeartRateMonitor.Cqrs.Queries
{
    public class GetMeasurementProcessSettingsQuery : IRequest<MeasurementProcessSettings>
    {
        
    }
}