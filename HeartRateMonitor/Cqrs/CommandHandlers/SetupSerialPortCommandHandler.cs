﻿using System;
using System.Threading;
using System.Threading.Tasks;
using HeartRateMonitor.Cqrs.Commands;
using HeartRateMonitor.Models;
using HeartRateMonitor.Services.Interfaces;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace HeartRateMonitor.Cqrs.CommandHandlers
{
    public class SetupSerialPortCommandHandler : IRequestHandler<SetupSerialPortCommand, SerialPortSettings>
    {
        #region Properties

        private readonly ISerialPortService _serialPortService;

        #endregion


        #region Constructor

        public SetupSerialPortCommandHandler(IServiceProvider serviceProvider)
        {
	        _serialPortService = serviceProvider.GetService<ISerialPortService>();
        }

        #endregion

        #region Methods

        public virtual Task<SerialPortSettings> Handle(SetupSerialPortCommand command, CancellationToken cancellationToken)
        {
	        _serialPortService.ConnectAsync(command);
            return Task.FromResult((SerialPortSettings)command);
        }

        #endregion
    }
}