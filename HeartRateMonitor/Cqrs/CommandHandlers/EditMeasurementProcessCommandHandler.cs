﻿using System;
using System.Threading;
using System.Threading.Tasks;
using HeartRateMonitor.Cqrs.Commands;
using HeartRateMonitor.Models;
using HeartRateMonitor.Services.Interfaces;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace HeartRateMonitor.Cqrs.CommandHandlers
{
	public class EditMeasurementProcessCommandHandler
		: IRequestHandler<EditMeasurementProcessCommand, MeasurementProcessSettings>
	{
		#region Properties

		private readonly IMeasurementProcessService _measurementProcessService;

		#endregion

		#region Constructor

		public EditMeasurementProcessCommandHandler(IServiceProvider serviceProvider)
		{
			_measurementProcessService = serviceProvider.GetService<IMeasurementProcessService>();
		}

		#endregion

		#region Methods

		public virtual async Task<MeasurementProcessSettings> Handle(EditMeasurementProcessCommand request, CancellationToken cancellationToken)
		{
			// Get the current measurement settings.
			var settings = await _measurementProcessService.GetSettingsAsync();

			if (request.SlaveId != null && request.SlaveId.HasModified)
				settings.SlaveId = request.SlaveId.Value;

			if (request.Interval != null && request.Interval.HasModified)
				settings.Interval = request.Interval.Value;

			if (request.Timeout != null && request.Timeout.HasModified)
				settings.Timeout = request.Timeout.Value;

			await _measurementProcessService.UpdateSettingsAsync(settings);
			return settings;
		}

		#endregion
	}
}