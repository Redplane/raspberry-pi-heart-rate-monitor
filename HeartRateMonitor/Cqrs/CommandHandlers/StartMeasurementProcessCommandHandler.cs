﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using HeartRateMonitor.Constants;
using HeartRateMonitor.Cqrs.Commands;
using HeartRateMonitor.Models;
using HeartRateMonitor.Models.Messages;
using HeartRateMonitor.Services.Interfaces;
using LiteMessageBus.Services.Interfaces;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace HeartRateMonitor.Cqrs.CommandHandlers
{
	public class StartMeasurementProcessCommandHandler : IRequestHandler<StartMeasurementProcessCommand>
	{
		#region Properties

		private readonly IMeasurementProcessService _measurementProcessService;

		private readonly ILiteMessageBusService _messageBusService;

        private readonly ISerialPortService _serialPortService;

		#endregion

		#region Constructor

		public StartMeasurementProcessCommandHandler(IServiceProvider serviceProvider)
		{
			_measurementProcessService = serviceProvider.GetService<IMeasurementProcessService>();
			_messageBusService = serviceProvider.GetService<ILiteMessageBusService>();
            _serialPortService = serviceProvider.GetService<ISerialPortService>();
        }

		#endregion

		#region Methods

		public virtual async Task<Unit> Handle(StartMeasurementProcessCommand request,
			CancellationToken cancellationToken)
        {
            var modbusMaster = _serialPortService.GetModBusRtuMaster();
			if (modbusMaster == null)
				throw new HttpResponseException(HttpStatusCode.Forbidden, "INVALID_SERIAL_PORT_SETTINGS", "Invalid serial port settings");

			var settings = await _measurementProcessService.GetSettingsAsync();
			var message = new MeasurementProcessStartedMessage(settings);

			_messageBusService.AddMessage(MessageChannelNames.MeasurementProcess,
				MessageEventNames.MeasurementProcessStarted, message);

			return Unit.Value;
		}

		#endregion
	}
}