﻿using System;
using System.Threading;
using System.Threading.Tasks;
using HeartRateMonitor.Cqrs.Commands;
using HeartRateMonitor.Services.Interfaces;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace HeartRateMonitor.Cqrs.CommandHandlers
{
	public class StopMeasurementProcessCommandHandler : IRequestHandler<StopMeasurementProcessCommand>
	{
		#region Properties

		private readonly IMeasurementProcessService _measurementProcessService;

		private readonly IMeasurementService _measurementService;

		private readonly IEcgService _ecgService;

		#endregion

		#region Constructor

		public StopMeasurementProcessCommandHandler(IServiceProvider serviceProvider)
		{
			_measurementProcessService = serviceProvider.GetService<IMeasurementProcessService>();
			_measurementService = serviceProvider.GetService<IMeasurementService>();
			_ecgService = serviceProvider.GetService<IEcgService>();
		}

		#endregion

		#region Methods

		public virtual Task<Unit> Handle(StopMeasurementProcessCommand request, CancellationToken cancellationToken)
		{
			_measurementProcessService.Stop();

			// Submit the max value which has been measured.
			var value = _measurementService.GetMaxMeasurementValue();
			if (value != null)
			{
				var bpm = _measurementService.ToBpm(value.Value);
				_ecgService.PublishMaxMeasurementToIotHub(bpm);
			}

			return Task.FromResult(Unit.Value);
		}

		#endregion
	}
}