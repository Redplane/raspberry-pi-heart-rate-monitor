﻿using HeartRateMonitor.Models;
using HeartRateMonitor.ViewModels;
using MediatR;

namespace HeartRateMonitor.Cqrs.Commands
{
	public class EditMeasurementProcessCommand : IRequest<MeasurementProcessSettings>
	{
		#region Properties

		public EditableFieldViewModel<int> SlaveId { get; set; }

		public EditableFieldViewModel<int> Interval { get; set; }

		public EditableFieldViewModel<int?> Timeout { get; set; }
		
		#endregion
	}
}