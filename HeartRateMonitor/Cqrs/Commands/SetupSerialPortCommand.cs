﻿using System.IO.Ports;
using HeartRateMonitor.Models;
using MediatR;

namespace HeartRateMonitor.Cqrs.Commands
{
    public class SetupSerialPortCommand : SerialPortSettings, IRequest<SerialPortSettings>
    {
        #region Constructor

        public SetupSerialPortCommand(string name, int baudRate, int dataBit, Parity parity, StopBits stopBits) : base(name, baudRate, dataBit, parity, stopBits)
        {
        }

        #endregion
    }
}