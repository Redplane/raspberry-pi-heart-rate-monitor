﻿using System;
using System.IO.Ports;
using System.Threading.Tasks;
using HeartRateMonitor.Models;
using HeartRateMonitor.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace HeartRateMonitor.Services
{
    public class InOutPortService : IInOutPortService
    {
        #region Properties

        //private readonly ILiteMessageBusService _messageBusService;

        private SerialPortSettings _serialPortSettings;

        private SerialPort _serialPort;

        #endregion

        #region Constructor

        public InOutPortService(IServiceProvider serviceProvider)
        {
            // Services retrieval.
            //_messageBusService = serviceProvider.GetService<ILiteMessageBusService>();
        }

        #endregion

        #region Methods

        public virtual Task<SerialPortSettings> GetSettingsAsync()
        {
            return Task.FromResult(_serialPortSettings);
        }

        public virtual Task<bool> SetupSerialPortAsync(SerialPortSettings settings)
        {
            try
            {
                _serialPortSettings = settings;

                if (_serialPort != null && _serialPort.IsOpen)
                    _serialPort.Close();

                _serialPort = new SerialPort(settings.Name, settings.BaudRate, settings.Parity, settings.DataBit,
                    settings.StopBits);
                _serialPort.Open();

                return Task.FromResult(true);
            }
            catch
            {
                return Task.FromResult(false);
            }

        }

        #endregion
    }
}