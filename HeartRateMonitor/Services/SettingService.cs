﻿using System.Threading.Tasks;
using HeartRateMonitor.Models;
using HeartRateMonitor.Services.Interfaces;

namespace HeartRateMonitor.Services
{
	public class SettingService : ISettingService
	{
		public virtual async Task SaveSerialPortSettingsAsync(SerialPortSettings settings)
		{
			throw new System.NotImplementedException();
		}

		public virtual async Task<SerialPortSettings> GetSerialPortSettingsAsync()
		{
			throw new System.NotImplementedException();
		}

		public virtual async Task SaveMeasurementProcessSettingsAsync(MeasurementProcessSettings settings)
		{
			throw new System.NotImplementedException();
		}

		public virtual async Task<MeasurementProcessSettings> GetMeasurementProcessSettingsAsync()
		{
			throw new System.NotImplementedException();
		}
	}
}