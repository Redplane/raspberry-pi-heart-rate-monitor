﻿using HeartRateMonitor.Services.Interfaces;

namespace HeartRateMonitor.Services
{
	public class MeasurementService : IMeasurementService
	{
		#region Properties

		/// <summary>
		/// Max value which has been measured.
		/// </summary>
		private int? _maxMeasurementValue = null;

		private int _maxBpm = 200;

		#endregion

		#region Methods

		public virtual void SaveMaxMeasurementValue(int? value)
		{
			if (value == null)
			{
				_maxMeasurementValue = null;
				return;
			}

			if (value < _maxMeasurementValue)
				return;

			_maxMeasurementValue = value;
		}

		public virtual int? GetMaxMeasurementValue()
		{
			return _maxMeasurementValue;
		}

		public int ToBpm(int value)
		{
			if (value < 1)
				return 0;

			var bpm = (double)_maxBpm * value / 100;
			return (int)bpm;
		}

		#endregion
	}
}