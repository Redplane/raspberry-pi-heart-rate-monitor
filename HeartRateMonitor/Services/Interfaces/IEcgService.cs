﻿using System;

namespace HeartRateMonitor.Services.Interfaces
{
	public interface IEcgService
	{
		#region Properties

		/// <summary>
		/// Publish ecg measurement to IoT hub
		/// </summary>
		/// <param name="dateTime"></param>
		/// <param name="value"></param>
		void PublishMeasurementToIotHub(DateTime dateTime, int value);

		/// <summary>
		/// Publis max ecg measurement to IoT hub
		/// </summary>
		/// <param name="value"></param>
		void PublishMaxMeasurementToIotHub(int value);


		#endregion
	}
}