﻿using System;
using System.Threading.Tasks;
using HeartRateMonitor.Enums;
using HeartRateMonitor.Models;

namespace HeartRateMonitor.Services.Interfaces
{
    public interface IMeasurementProcessService
    {
        #region Methods

        /// <summary>
        /// Start measurement process
        /// </summary>
        void Start(Action action, MeasurementProcessSettings measurementProcessSettings);

        /// <summary>
        /// Stop measurement process
        /// </summary>
        void Stop();

        /// <summary>
        /// Measurement status.
        /// </summary>
        /// <returns></returns>
        MeasurementProcessStatuses GetStatus();

        Task<MeasurementProcessSettings> GetSettingsAsync();

        Task UpdateSettingsAsync(MeasurementProcessSettings settings);


        #endregion
    }
}