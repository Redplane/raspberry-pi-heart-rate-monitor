﻿using System.Threading.Tasks;
using HeartRateMonitor.Models;

namespace HeartRateMonitor.Services.Interfaces
{
	public interface ISettingService
	{
		#region Methods

		Task SaveSerialPortSettingsAsync(SerialPortSettings settings);

		Task<SerialPortSettings> GetSerialPortSettingsAsync();

		Task SaveMeasurementProcessSettingsAsync(MeasurementProcessSettings settings);

		Task<MeasurementProcessSettings> GetMeasurementProcessSettingsAsync();

		#endregion
	}
}