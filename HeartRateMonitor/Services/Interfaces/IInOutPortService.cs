﻿using System.Threading.Tasks;
using HeartRateMonitor.Models;

namespace HeartRateMonitor.Services.Interfaces
{
    public interface IInOutPortService
    {
        #region Methods

        /// <summary>
        /// Get serial port setting which is for sending data as output.
        /// </summary>
        /// <returns></returns>
        Task<SerialPortSettings> GetSettingsAsync();

        /// <summary>
        /// Setup serial port asynchronously.
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        Task<bool> SetupSerialPortAsync(SerialPortSettings settings);

        #endregion
    }
}