using System.IO.Ports;
using System.Threading.Tasks;
using HeartRateMonitor.Models;
using NModbus;

namespace HeartRateMonitor.Services.Interfaces
{
    public interface ISerialPortService
    {
        #region Methods

        /// <summary>
        ///     Connect to serial port asynchronously.
        /// </summary>
        /// <returns></returns>
        Task ConnectAsync(SerialPortSettings settings);

        /// <summary>
        /// Disconnect serial port connection asynchronously.
        /// </summary>
        /// <returns></returns>
        Task DisconnectAsync();

        /// <summary>
        /// Get available ports in the system.
        /// </summary>
        /// <returns></returns>
        Task<string[]> GetAvailablePortsAsync();

        /// <summary>
        /// Get the ModBus RTU master instance.
        /// </summary>
        /// <returns></returns>
        IModbusMaster GetModBusRtuMaster();

        SerialPortSettings GetSettings();

        /// <summary>
        /// Whether specific port has connected or not.
        /// </summary>
        /// <returns></returns>
        Task<bool> IsConnectedAsync();

        SerialPort GetSerialPort();

        #endregion
    }
}
