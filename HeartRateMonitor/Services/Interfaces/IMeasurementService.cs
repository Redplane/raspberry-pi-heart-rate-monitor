﻿namespace HeartRateMonitor.Services.Interfaces
{
	public interface IMeasurementService
	{
		#region Methods

		/// <summary>
		/// Save max measurement value.
		/// </summary>
		/// <param name="value"></param>
		void SaveMaxMeasurementValue(int? value);

		/// <summary>
		/// Get max measurement value which has been done.
		/// </summary>
		/// <returns></returns>
		int? GetMaxMeasurementValue();

		/// <summary>
		/// Convert percentage to bpm
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		int ToBpm(int value);

		#endregion
	}
}