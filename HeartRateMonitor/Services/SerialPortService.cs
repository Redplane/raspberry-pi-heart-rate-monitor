using System;
using System.IO.Ports;
using System.Threading.Tasks;
using HeartRateMonitor.Constants;
using HeartRateMonitor.Enums;
using HeartRateMonitor.Models;
using HeartRateMonitor.Models.Messages;
using HeartRateMonitor.Services.Interfaces;
using LiteMessageBus.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using NModbus;
using NModbus.Serial;

namespace HeartRateMonitor.Services
{
	public class SerialPortService : ISerialPortService
	{
		#region Constructor

		public SerialPortService(IServiceProvider serviceProvider)
		{
			_messageBusService = serviceProvider.GetService<ILiteMessageBusService>();
		}

		#endregion

		#region Properties

		private SerialPort _serialPort;

		private IModbusMaster _modbusMaster;

		/// <summary>
		///     Service to publish message.
		/// </summary>
		private readonly ILiteMessageBusService _messageBusService;

		#endregion

		#region Methods

		/// <summary>
		///     <inheritdoc />
		/// </summary>
		/// <returns></returns>
		public virtual Task ConnectAsync(SerialPortSettings settings)
		{
			if (_serialPort != null && _serialPort.IsOpen)
			{
				_messageBusService.AddMessage(MessageChannelNames.Connection,
					MessageEventNames.SerialPortConnectionStatusChanged,
					new SerialPortConnectionStatusChangedMessage(SerialPortConnectionStatuses.Disconnected));
				_serialPort.Close();
			}

			_serialPort = new SerialPort(settings.Name, settings.BaudRate, settings.Parity,
				settings.DataBit, settings.StopBits);

			try
			{
				// Try to open port.
				_serialPort.Open();

				// Initialize modbus master instant.
				var adapter = new SerialPortAdapter(_serialPort);
				//adapter.ReadTimeout = 1000;
				//adapter.WriteTimeout = 1000;

				var factory = new ModbusFactory();
				_modbusMaster = factory.CreateRtuMaster(adapter);

				// Publish a message about connection opened.
				_messageBusService.AddMessage(MessageChannelNames.Connection,
					MessageEventNames.SerialPortConnectionStatusChanged,
					new SerialPortConnectionStatusChangedMessage(SerialPortConnectionStatuses.Connected));
			}
			catch
			{
				_modbusMaster = null;
			}
			

			return Task.CompletedTask;
		}

		/// <summary>
		///     <inheritdoc />
		/// </summary>
		/// <returns></returns>
		public virtual Task DisconnectAsync()
		{
			_serialPort.Close();

			// Publish a message about connection opened.
			_messageBusService.AddMessage(MessageChannelNames.Connection,
				MessageEventNames.SerialPortConnectionStatusChanged,
				new SerialPortConnectionStatusChangedMessage(SerialPortConnectionStatuses.Disconnected));

			return Task.CompletedTask;
		}

		/// <summary>
		///     <inheritdoc />
		/// </summary>
		/// <returns></returns>
		public virtual Task<string[]> GetAvailablePortsAsync()
		{
			var names = SerialPort.GetPortNames();
			return Task.FromResult(names);
		}

		/// <summary>
		///     <inheritdoc />
		/// </summary>
		/// <returns></returns>
		public virtual IModbusMaster GetModBusRtuMaster()
		{
			return _modbusMaster;
		}

		public virtual SerialPortSettings GetSettings()
		{
			if (_serialPort == null)
				return null;

			var serialPortSettings = new SerialPortSettings(_serialPort.PortName, _serialPort.BaudRate, _serialPort.DataBits, _serialPort.Parity, _serialPort.StopBits);
			return serialPortSettings;
		}

		/// <summary>
		///     <inheritdoc />
		/// </summary>
		/// <returns></returns>
		public virtual Task<bool> IsConnectedAsync()
		{
			return Task.FromResult(_serialPort.IsOpen);
		}

		public SerialPort GetSerialPort()
		{
			return _serialPort;
		}

		#endregion

		#region Internal methods

		#endregion
	}
}