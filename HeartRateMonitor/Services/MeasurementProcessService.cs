﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using HeartRateMonitor.Enums;
using HeartRateMonitor.Models;
using HeartRateMonitor.Services.Interfaces;

namespace HeartRateMonitor.Services
{
	public class MeasurementProcessService : IMeasurementProcessService
	{
		#region Constructor

		public MeasurementProcessService()
		{
		}

		#endregion

		#region Properties

		private CancellationTokenSource _cancellationTokenSource;

		private MeasurementProcessSettings _settings;

		#endregion

		#region Methods

		public void Start(Action action, MeasurementProcessSettings settings)
		{
			_cancellationTokenSource?.Cancel();
			_cancellationTokenSource = new CancellationTokenSource();

			var timeout = settings.Interval;
			if (settings.Timeout != null && settings.Timeout < settings.Interval)
				timeout = settings.Interval;

			var intervalTimeSpan = TimeSpan.FromMilliseconds(settings.Interval);

			Task.Run(async () =>
			{
				var stopwatch = new Stopwatch();
				stopwatch.Start();

				while (!_cancellationTokenSource.IsCancellationRequested)
				{
					try
					{
						stopwatch.Reset();

						var actionTask = Task.Run(action.Invoke, _cancellationTokenSource.Token);
						var timeoutTask = Task.Delay(TimeSpan.FromMilliseconds(timeout));

						await Task.WhenAny(new[] { actionTask, timeoutTask });

						// Get current system time.
						stopwatch.Stop();

						// Get the remaining time.
						if (stopwatch.Elapsed > intervalTimeSpan)
							continue;

						await Task.Delay(intervalTimeSpan - stopwatch.Elapsed);
					}
					catch
					{
						// Suppress the exception and continue.
						await Task.Delay(TimeSpan.FromMilliseconds(timeout));
					}
				}

				stopwatch.Stop();

			}, _cancellationTokenSource.Token);
		}

		public void Stop()
		{
			_cancellationTokenSource?.Cancel();
		}

		public MeasurementProcessStatuses GetStatus()
		{
			if (_cancellationTokenSource == null)
				return MeasurementProcessStatuses.Unavailable;

			if (_cancellationTokenSource.IsCancellationRequested)
				return MeasurementProcessStatuses.Stopped;

			return MeasurementProcessStatuses.Running;
		}

		/// <summary>
		///     Get measurement process settings asynchronously
		/// </summary>
		/// <returns></returns>
		public virtual Task<MeasurementProcessSettings> GetSettingsAsync()
		{
			var defaultSettings = MeasurementProcessSettings.GetDefaultSettings();

			if (_settings == null)
				_settings = defaultSettings;

			if (_settings.Interval < 100)
				_settings = defaultSettings;

			return Task.FromResult(_settings);
		}

		public virtual Task UpdateSettingsAsync(MeasurementProcessSettings settings)
		{
			if (_settings == null)
			{
				_settings = MeasurementProcessSettings.GetDefaultSettings();
				return Task.CompletedTask;
			}

			_settings = settings;
			return Task.CompletedTask;
		}

		#endregion
	}
}