﻿using System;
using System.Text;
using System.Threading;
using HeartRateMonitor.Models;
using HeartRateMonitor.Services.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using uPLibrary.Networking.M2Mqtt;

namespace HeartRateMonitor.Services
{
	public class EcgService : IEcgService
	{
		#region Properties

		private readonly MqttClient _mqttClient;

		private readonly string _clientId;

		private readonly JsonSerializerSettings _jsonSerializerSettings;

		private readonly IotHubSettings _settings;

		#endregion

		#region Constructor

		public EcgService(MqttClient mqttClient, IotHubSettings iotHubSettings)
		{
			_mqttClient = mqttClient;
			_clientId = Guid.NewGuid().ToString("N");
			_settings = iotHubSettings;

			_jsonSerializerSettings = new JsonSerializerSettings();
			var contractResolver = new DefaultContractResolver();
			contractResolver.NamingStrategy = new SnakeCaseNamingStrategy();
			_jsonSerializerSettings.ContractResolver = contractResolver;
		}

		#endregion

		#region Methods

		public void PublishMeasurementToIotHub(DateTime dateTime, int value)
		{
			if (_mqttClient == null || !_mqttClient.IsConnected)
				return;

			if (value < 0)
				return;

			var ecgMeasurement = new EcgMeasurement(DateTime.UtcNow, value);
			var szMessage = JsonConvert.SerializeObject(ecgMeasurement, _jsonSerializerSettings);
			_mqttClient.Publish(_settings.MeasurementTopic, Encoding.UTF8.GetBytes(szMessage));
			Console.WriteLine($"Published: {szMessage}");
		}

		public virtual void PublishMaxMeasurementToIotHub(int value)
		{
			if (_mqttClient == null || !_mqttClient.IsConnected)
				return;

			if (value < 0)
				return;

			var ecgMeasurement = new MaxHeartRateMeasurement(DateTime.UtcNow, value);
			var szMessage = JsonConvert.SerializeObject(ecgMeasurement, _jsonSerializerSettings);
			_mqttClient.Publish(_settings.MaxHeartRateTopic, Encoding.UTF8.GetBytes(szMessage));
			Console.WriteLine($"Published: {szMessage}");
		}

		#endregion
	}
}