rmdir /Q /S obj
rmdir /Q /S bin

dotnet build
electronize build /target linux /PublishReadyToRun false

dotnet publish -r linux-arm --output "obj\desktop\linux-arm\bin"

xcopy .\obj\desktop\linux\api .\obj\desktop\linux-arm\api\
xcopy .\obj\desktop\linux\main.js .\obj\desktop\linux-arm\
xcopy .\obj\desktop\linux\package.json .\obj\desktop\linux-arm\
xcopy .\obj\desktop\linux\package-lock.json .\obj\desktop\linux-arm\

cd .\obj\desktop\linux-arm

npm install

electron-packager . --platform=linux --arch=armv7l --overwrite --out="..\..\..\bin\desktop-arm"

echo "DONE"